import yaml
import inspect


aoo_mapping = yaml.load(open('aoo-ifi-hs-mapping.yaml'))

outputtable = aoo_mapping.pop(0)
query_tablename = aoo_mapping.pop(0)


#Generate SQL statements 
def generate_update_statement(tablename,fieldname,listofvals,invert=False,newvalue=False):
    """generate a SQL update statement that maps variables to lowest included value"""
    if newvalue:
        firstvalue = newvalue
    else:
        firstvalue = sorted(listofvals)[0]
    args = {'tablename':tablename, 'fieldname':fieldname,
            'listofvals':"','".join(str(i) for i in listofvals), 
            'firstvalue':firstvalue}
    if not invert:
        formatstring =inspect.cleandoc(#
        """UPDATE {tablename} SET {fieldname} = '{firstvalue}' 
        WHERE {fieldname} IN ('{listofvals}');""".format(**args))
    else:
        formatstring =inspect.cleandoc(#
        """UPDATE {tablename} SET {fieldname} = '{firstvalue}' 
        WHERE {fieldname} NOT IN ('{listofvals}');""".format(**args))
    return formatstring

#create table if it does not exist
table_create = inspect.cleandoc(#
    """DROP TABLE IF EXISTS {0};
       CREATE TABLE {0} AS
       (SELECT * FROM {1});""".format(outputtable,query_tablename))
sqloutput = []
sqloutput.append('BEGIN;')
for concept in aoo_mapping:
    concitems = []
    fieldname = concept['fieldname']
    try: 
        for item in concept['exclude'].values():
            concitems.append(item)
        excludeitems = [item for sublist in concitems for item in sublist]
        exclout = generate_update_statement(outputtable,fieldname,excludeitems)
        sqloutput.append(exclout)
    except KeyError:
        pass
    for item in concept['include'].values():
        concitems.append(item)
        out = generate_update_statement(outputtable,fieldname,item)
        sqloutput.append(out)
        catchitems = [item for sublist in concitems for item in sublist]
    catchall = generate_update_statement(outputtable,fieldname,catchitems,
                                         invert=True,newvalue='00')
    sqloutput.append(catchall)
sqloutput.append('COMMIT;')
with open('aoo-ifi-sql-map.sql','wb') as f:
    f.writelines("{}\n".format(outputline) for outputline in sqloutput)
