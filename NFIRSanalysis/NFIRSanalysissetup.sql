begin;
drop table if exists onefamdetfires;
create table onefamdetfires as (
select * from allfire WHERE prop_use = '419' and version = '5.0' and aid not in ('3','4') and (((inc_date between '20020101' and '20071231' and inc_type IN ('111','112','120','121','122','123')) or (inc_date between '20080101' and '20121231' and inc_type in ('111','120','121','122','123')) and struc_type in ('1','2')) OR (inc_type IN ('113','114','115','116','117','118') and (struc_type IN ('1','2') or struc_type is null))));
commit;
