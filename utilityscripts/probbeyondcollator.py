import sys
import os
import subprocess
import pandas as pd
import glob

def main(filesdirectory,searchterm):
    filelist = glob.glob('/'.join((os.path.abspath(filesdirectory),searchterm)))
    outframe = pd.DataFrame()
    for item in filelist:
        inframe = pd.read_pickle(item)
        outframe = outframe.append(inframe)
    outframe.to_csv('/'.join((os.path.abspath(filesdirectory),'pbcurvecollection.csv')))

if __name__ == "__main__":
    repo_dir = subprocess.Popen(['git', 'rev-parse', '--show-toplevel'], stdout=subprocess.PIPE).communicate()[0].rstrip()
    os.chdir(repo_dir)
    filesdirectory = os.path.abspath('./outputs/damagecurves/')
    searchterm = '*probbeyondcurve.p'
    print sys.argv
    if len(sys.argv) >= 2:
        filesdirectory = sys.argv[1]
    if len(sys.argv) == 3:
        searchterm = sys.argv[2]
    if len(sys.argv) > 3:
        print sys.argv
        raise Exception("Too many arguments submitted to script")
    main(filesdirectory,searchterm)
