import itertools
import subprocess
import collections
import copy
import yaml
import sys
import os

#helper functions for manipulating the configuration files

def keypaths(k, nested):
    """generates list of keys required to reach 'k,' a search key"""
    for key, value in nested.iteritems():
        if k == key:
            yield [key]
        else: 
            if isinstance(value, collections.Mapping):
                for subkey in keypaths(k,value):
                    yield [key] + subkey


def nested_return(dic,keys,value):
    retdic = copy.deepcopy(dic)
    nested_set(retdic,keys,value)
    return retdic


def nested_set(dic, keys, value):
    for key in keys[:-1]:
        dic = dic.setdefault(key, {})
    dic[keys[-1]] = value

#logic functions for handling parameter sweep inputs

def flatten(l):
    for el in l:
        if isinstance(el, collections.Iterable) and not isinstance(el, basestring):
            for sub in flatten(el):
                yield sub
        else:
            yield el

def joinlistcheck(paramdict,joinlist):
    oldcheckval = len(paramdict[joinlist[0]])
    for item in joinlist:
        if len(paramdict[item]) != oldcheckval:
            raise Exception("Keys in joinlist are not all the same value!")
    return

def inputdictbuild(paramdict, crosslist=[], joinlist=[]):
    outdict = []
    if joinlist in crosslist:
        crossout = [a for a in crosslist if a is not joinlist]
        crossflat = joinlist+crossout
        crossinterm =  list(itertools.product(zip(*(paramdict[k] for k in joinlist)),*[paramdict[a] for a in crossout]))
        crossdict = [dict(zip(crossflat,flatten(vals))) for vals in crossinterm]
        outdict = outdict+crossdict
    else:
        if crosslist:
            crossdict = [dict(zip(crosslist,vals)) for vals in itertools.product(*[paramdict[a] for a in crosslist])]
        else:
            crossdict = []
        if joinlist:
            joindict = [dict(zip(joinlist,vals)) for vals in zip(*(paramdict[k] for k in joinlist))]
        else:
            joindict = []
        outdict = outdict+crossdict+joindict
    for key, value in paramdict.items():
        if key not in joinlist+[a for a in crosslist if a is not joinlist]:
            outdict = outdict + [{key:vals} for vals in value]
    return outdict

#generate the config files according to proper file convention here.
#file convention is "changedparameter1-value-changedparameter2-value-...-modelconfig.yaml
#Note: input config file is considered to contain the "default parameters"

def configbuild(inputconfig, paramsweepdict,joinlist=[],crosslist=[],
                configoutputdir='./outputs/sensitivityconfigs/',
                damagecurveoutputdir='./outputs/damagecurves/'):
    """generate config files according to preferred parameter sweep"""
    joinlistcheck(paramsweepdict,joinlist)
    inputdict = inputdictbuild(paramsweepdict,crosslist,joinlist)
    for dictionary in inputdict:
        outdict = copy.deepcopy(inputconfig)
        outputnamepieces = []
        for key, item in dictionary.items():
            #perform parameter substitution on output file
            nested_set(outdict,keypaths(key,outdict).next(),item)
            #take care of housekeeping tasks
            outputnamepieces = outputnamepieces + ['_'.join(str(i) for i in [key,item])] #filename convention follows relevant parameters
            
        outputname = '-'.join(outputnamepieces)
        dcurveoutloc = ''.join((damagecurveoutputdir,
                                '-'.join((outputname,'damagecurves.p'))))
        dcurvewproboutloc = ''.join((damagecurveoutputdir,
                                     '-'.join((outputname,'damagecurveswprobs.p'))))
        #edit the relevant output fields of outdict
        #for now, we use the damagecurve output/input fields
        #damagecalculator
        outputlocationpath = ['damagecalculator','outputlocation']
        nested_set(outdict,outputlocationpath,dcurveoutloc)
        #cond-prob-construct
        damagecurvelocpath = ['cond-prob-construct','damagecurveloc']
        outputfilenamepath = ['cond-prob-construct','outputfilename']
        nested_set(outdict,damagecurvelocpath,dcurveoutloc)
        nested_set(outdict,outputfilenamepath,dcurvewproboutloc)
        #configfile output information
        configoutpath = ''.join((configoutputdir,'-'.join((outputname,'conf.yaml'))))
        yield configoutpath, outdict


def configwriter(configbuildgenerator):
    for configoutpath, outdict in configbuildgenerator:
        with open(configoutpath,'wb') as f:
            yaml.dump(outdict,f,default_flow_style=False)

def main(inputconfigloc,paramdict,joinlist=[],crosslist=[],
         configoutputdir='./outputs/sensitivityconfigs/',
         damagecurveoutputdir='./outputs/damagecurves/'):
    inputconfig = yaml.load(open(inputconfigloc))
    output = configbuild(inputconfig,paramdict,joinlist,crosslist,
                         configoutputdir,damagecurveoutputdir)
    configwriter(output)

if __name__ == "__main__":
    repo_dir = subprocess.Popen(['git', 'rev-parse', '--show-toplevel'], stdout=subprocess.PIPE).communicate()[0].rstrip()
    os.chdir(repo_dir)
    if len(sys.argv) == 2:
        configlocation = os.path.abspath(sys.argv[1])
    elif len(sys.argv) > 2:
        raise Exceptions("Too many system arguments submitted to script")
    else:
        configlocation = os.path.abspath('./modelconfig.yaml')
    #outline which parameters y u want searched here
    #initial functionality does not include matrix variation
    #if you want parameters to vary jointly (e.g. different krhoc and E)
    #use the appropriate function
    paramdict = {'krhoc': [0.036,0.115,0.59,0.00624],   #polyurethane, wood, polyester, cotton
                'E': [67e3, 124.7e3, 180.e3,153.1e3],  #
                'Tign': [643.0,563.0,680.0,602.0],    #
                'qdub': [15.,25],               #
                }
    joinlist = ['krhoc','E','Tign']
    crosslist = ['qdub',joinlist]

    #main function
    main(configlocation,paramdict,joinlist,crosslist)
