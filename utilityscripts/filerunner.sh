#!/bin/bash

usage() { echo "Usage: $0 [-l <logfile path/name>] [-j <# parallel jobs>] [-t <timeout (seconds)>] [joblist]" 1>&2; exit 1; }

logfile=./runlog.log

while getopts ":l:j:t:" o; do
    case "${o}" in
        l)
            #stands for logfile
            logfile=`readlink -f ${OPTARG}`
            ;;
        j)
            #number of jobs to run in parallel at a time
            njobs=${OPTARG}
            ;;
        t)  #timeout duration
            timeout=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $(($OPTIND-1))

if [ -z ${njobs+x} ] && [ -z ${timeout+x} ]; then
    parallel --joblog ${logfile} --eta < $1
elif [ -z ${timeout+x} ]; then
    parallel --joblog ${logfile} -j${njobs} --eta < $1
elif [ -z ${njobs+x} ]; then
    parallel --joblog ${logfile} --timeout ${timeout} --eta < $1
else
    parallel --joblog ${logfile} -j${njobs} --timeout ${timeout} --eta < $1
fi
