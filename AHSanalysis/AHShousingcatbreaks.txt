note: breaks based around 0.1,0.5,0.9 percentiles

Detached housing

unitsf

<=1000

1001-1800

1801-3200

>3200

bedrooms

0-2

3

4+

bathrooms

1

2

3

Apartments

unitsf

<500

501-850

851-1500

1500+

bedroomsgf
1

2

3+

bathrooms
1,2+
