#!/bin/bash

usage() { echo "Usage: $0 [-t <output filepath>] [-s <source directory>] [-e <extension>] [arg1...argN] note that arguments are collapsed and prepended to source directory files" 1>&2; exit 1; }

outputpathfile=./outputs/joblist

while getopts ":t:s:e:" o; do
    case "${o}" in
        t)
            #stands for target
            outputpathfile=`readlink -f ${OPTARG}`
            ;;
        s)
            #stands for source directory
            sourcedirectory=`readlink -f ${OPTARG}`
	    ;;
        e)
            #stands for extension
            #excluding this just takes the whole directory or throws an error
            #if the unpack is too large
            extensionsearch=${OPTARG}
	    ;;
        *)
            usage
            ;;
    esac
done
shift $(($OPTIND-1))

PREPEND=""

for var in "$@";
do
    if [[ -d $var ]] || [[ -f $var ]]; then
        ABS=`readlink -f $var`
        PREPEND="${PREPEND} ${ABS}"
    else
        PREPEND="${PREPEND} ${var}"
    fi
done


ls -d ${sourcedirectory}/*${extensionsearch} > ${outputpathfile}
sed "s|^|${PREPEND} |" ${outputpathfile} > tmp && mv tmp ${outputpathfile}
