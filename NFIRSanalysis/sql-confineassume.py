import yaml
import inspect

def fieldstringbuild(fielddict):
    return '\', '.join('=\''.join(str(s) for s in i) for i in fielddict.items())

def generate_update_statement(tablename,fielddict,filterfield,filtervalue):
    """generate a SQL update statement from field:value dictionaries and filters"""
    updatestring = "UPDATE {} ".format(tablename)
    fieldstring = "SET "+fieldstringbuild(fielddict)+"\'"
    filterstring = " WHERE {} = '{}';".format(filterfield,filtervalue)
    return updatestring + fieldstring + filterstring

aoo_mapping = yaml.load(open('aoo-ifi-hs-mapping.yaml'))
operate_table = aoo_mapping.pop(0)
query_tablename = aoo_mapping.pop(0)
#create table if it does not exist
table_create = inspect.cleandoc(#
    """DROP TABLE IF EXISTS {0};
       CREATE TABLE {0} AS
       (SELECT * FROM {1});""".format(operate_table,query_tablename))
assume_operations = yaml.load(open('sql-confineprepmap.yaml'))
sqloutput = []
sqloutput.append('BEGIN;')
sqloutput.append(table_create)
for filterfield, value in assume_operations.items():
    for filtervalue, fielddict in value.items():
        sqloutput.append(generate_update_statement(operate_table,fielddict,filterfield,filtervalue))
sqloutput.append('COMMIT;')
with open('confineassumptions.sql','wb') as f:
    f.writelines("{}\n".format(outputline) for outputline in sqloutput)
