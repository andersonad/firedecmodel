#!/bin/bash

usage() { echo "Usage: $0 [-d <errorfiles directory>]" 1>&2; exit 1; }

while getopts ":d:" o; do
    case "${o}" in
        d)
            #stands for directory
            FILES=`readlink -f ${OPTARG}`
            ;;
        *)
            usage
            ;;
            
    esac
done
shift $((OPTIND-1))


for i in ${FILES}/*; do
    echo $i
    awk -F ',' -v OFS=',' '/COMPA/{$10="OFF"}{print}' $i > tmp && mv tmp $i
done
