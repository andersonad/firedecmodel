Residential 1 unit detached dwellings

1	1001-1800 sq. ft. 3 bed 1 floor
2	1800-3200 sq. ft. 4+bed 2 floor
3	1800-3200 sq. ft. 3 bed 2 floor
4	1001-1800 sq. ft. 3 bed 2 floor
5	1800-3200 sq. ft. 3 bed 1 floor

Apartments

1	501-850 sq. ft. 1 bed 1 bath
2	851-1500 sq. ft. 2 bed 1 bath
3	851-1500 sq. ft. 2 bed 2 bath
