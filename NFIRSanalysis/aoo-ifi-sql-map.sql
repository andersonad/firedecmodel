BEGIN;
DROP TABLE IF EXISTS onefamdetfiresproc;
CREATE TABLE onefamdetfiresproc AS
(SELECT * FROM onefamdetfires);
UPDATE onefamdetfiresproc SET area_orig = '00' 
WHERE area_orig IN ('81','82','83','84','85','86','80','31','32','33','34','35','36','37','38','30','72','76','77','78','26','11','12','13','15','16','17','10','71','73','74','75','41','43','44','45','46','40','91','92','93','94','95','96','97','98','90','24','25','01','02','03','04','05','09','23','47','00','28','20','51','52','53','54','55','56','58','50','61','62','63','64','65','66','67','68','60');
UPDATE onefamdetfiresproc SET area_orig = '' 
WHERE area_orig IN ('UU','uu',' ','');
UPDATE onefamdetfiresproc SET area_orig = '14' 
WHERE area_orig IN ('14');
UPDATE onefamdetfiresproc SET area_orig = '21' 
WHERE area_orig IN ('21','22','27','42');
UPDATE onefamdetfiresproc SET area_orig = '00' 
WHERE area_orig NOT IN ('81','82','83','84','85','86','80','31','32','33','34','35','36','37','38','30','72','76','77','78','26','11','12','13','15','16','17','10','71','73','74','75','41','43','44','45','46','40','91','92','93','94','95','96','97','98','90','24','25','01','02','03','04','05','09','23','47','00','28','20','51','52','53','54','55','56','58','50','61','62','63','64','65','66','67','68','60','UU','uu',' ','','14','21','22','27','42');
UPDATE onefamdetfiresproc SET first_ign = '00' 
WHERE first_ign IN ('41','42','43','44','45','46','47','40','71','72','73','74','75','77','70','24','26','20','99','81','82','83','84','85','86','87','88','11','12','13','14','15','16','17','18','10','51','52','53','54','55','56','57','58','59','50','33','37','38','30','00','76','61','62','63','64','65','66','67','68','60');
UPDATE onefamdetfiresproc SET first_ign = '' 
WHERE first_ign IN ('UU','uu','',' ');
UPDATE onefamdetfiresproc SET first_ign = '36' 
WHERE first_ign IN ('36');
UPDATE onefamdetfiresproc SET first_ign = '31' 
WHERE first_ign IN ('31','32');
UPDATE onefamdetfiresproc SET first_ign = '25' 
WHERE first_ign IN ('25');
UPDATE onefamdetfiresproc SET first_ign = '21' 
WHERE first_ign IN ('21');
UPDATE onefamdetfiresproc SET first_ign = '23' 
WHERE first_ign IN ('23');
UPDATE onefamdetfiresproc SET first_ign = '91' 
WHERE first_ign IN ('91','92','93','94','95','96','97');
UPDATE onefamdetfiresproc SET first_ign = '34' 
WHERE first_ign IN ('34');
UPDATE onefamdetfiresproc SET first_ign = '00' 
WHERE first_ign NOT IN ('41','42','43','44','45','46','47','40','71','72','73','74','75','77','70','24','26','20','99','81','82','83','84','85','86','87','88','11','12','13','14','15','16','17','18','10','51','52','53','54','55','56','57','58','59','50','33','37','38','30','00','76','61','62','63','64','65','66','67','68','60','UU','uu','',' ','36','31','32','25','21','23','91','92','93','94','95','96','97','34');
UPDATE onefamdetfiresproc SET heat_sourc = '' 
WHERE heat_sourc IN ('UU','uu','',' ');
UPDATE onefamdetfiresproc SET heat_sourc = '40' 
WHERE heat_sourc IN ('41','42','43','40');
UPDATE onefamdetfiresproc SET heat_sourc = '10' 
WHERE heat_sourc IN ('11','12','13','10');
UPDATE onefamdetfiresproc SET heat_sourc = '60' 
WHERE heat_sourc IN ('61','62','63','64','65','66','67','68','69','60');
UPDATE onefamdetfiresproc SET heat_sourc = '00' 
WHERE heat_sourc IN ('71','72','73','74','70','97','00');
UPDATE onefamdetfiresproc SET heat_sourc = '80' 
WHERE heat_sourc IN ('81','82','83','84','80');
UPDATE onefamdetfiresproc SET heat_sourc = '50' 
WHERE heat_sourc IN ('51','53','54','55','56','50');
UPDATE onefamdetfiresproc SET heat_sourc = '00' 
WHERE heat_sourc NOT IN ('UU','uu','',' ','41','42','43','40','11','12','13','10','61','62','63','64','65','66','67','68','69','60','71','72','73','74','70','97','00','81','82','83','84','80','51','53','54','55','56','50');
COMMIT;
