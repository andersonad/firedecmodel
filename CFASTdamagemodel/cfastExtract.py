import csv

#build cfastextract class (first seen in FDSconverter)
#include two-way dictionary dependency
class TwoWay:
    def __init__(self):
       self.d = {}
    def __len__(self):
        return(len(self.d)/2)
    def add(self, k, v):
       self.d[k] = v
       self.d[v] = k
    def remove(self, k):
       self.d.pop(self.d.pop(k))
    def get(self, k):
       return self.d[k]

class cfastExtract(object):
    def __init__(self,inputfile):
        self.times = None
        self.ambtemp = None
        self.ambpres = None
        self.elevation = None
        self.ambhumid = None
        self.eamb = None
        self.materials = {}
        self.rooms = {}
        self.hvents = {}
        self.vvents = {}
        self.fires = {}
        self.targets = {}
        self.ramps = TwoWay()
        self.chemi = TwoWay()
        with open(inputfile,'rb') as f:
            roomno=1
            ventno=1
            chemino=1
            rampno=1
            firename=None
            for row in f:
                rowline = [a.strip() for a in row.split(',')]
                if rowline[0]=='TIMES':
                    self.times = tuple(rowline[1:])
                if rowline[0]=='EAMB':
                    self.eamb = tuple(rowline[1:])
                if rowline[0]=='TAMB':
                    self.ambtemp = float(rowline[1])
                    self.ambpressure = float(rowline[2])
                    self.elevation = float(rowline[3])
                    self.ambhumid = float(rowline[4])
                if rowline[0]=='MATL':
                    matname = rowline[1]
                    rowlinereader = csv.reader(row.splitlines())
                    matrow = rowlinereader.next()[2:]
                    self.materials[matname] = tuple(matrow)
                if rowline[0]=='COMPA':
                    self.rooms[str(roomno)] = tuple(rowline[1:])
                    roomno +=1
                if rowline[0]=='HVENT':
                    self.hvents[ventno] = rowline[1:]
                    ventno +=1
                if rowline[0]=='FIRE':
                    firename=rowline[11]
                    firerow=tuple(rowline[1:11])
                if rowline[0]=='CHEMI':
                    chemirow = tuple(rowline[1:])
                    if chemirow not in self.chemi.d.keys():
                        cheminame='chemirow'+str(chemino)
                        self.chemi.add(chemirow,'chemirow'+str(chemino))
                        chemino+=1
                    else:
                        cheminame=self.chemi.get(chemirow)
                if rowline[0]=='TIME':
                    timerow = tuple(rowline[1:])
                if rowline[0]=='HRR':
                    hrr_row = tuple(rowline[1:])
                    ramprow = (timerow,hrr_row)
                    if ramprow not in self.ramps.d.keys():
                        rampname='ramprow'+str(rampno)
                        self.ramps.add(ramprow,'ramprow'+str(rampno))
                        rampno+=1
                    else:
                        rampname=self.ramps.get(ramprow)
                    self.fires[firename]=firerow+(cheminame,)+(rampname,)
                if rowline[0]=='TARGET':
                    targetname = rowline[12]
                    targetrow = tuple(rowline[1:12])
                    self.targets[targetname]=targetrow
