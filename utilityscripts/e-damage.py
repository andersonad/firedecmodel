import sys
import subprocess
import os
import glob
import pandas as pd
import numpy as np


def curvebuilder(dataframe,ndamagecurverows,timestep,probrowname,damagerowname):
    """build a damage curve, shape is a tuple a la numpy
    
       ndamagecurvetime::number of rows in damagecurve to include
    """
    outmat = np.zeros((len(dataframe),ndamagecurverows))
#     print outmat.shape
    for row in dataframe.itertuples():
#         print row.Index
        outmat[row.Index,:] = getattr(row,damagerowname)[:ndamagecurverows,1]*getattr(row,probrowname)
    outcurve = np.zeros([ndamagecurverows,2])
    outcurve[:,0] = range(0,ndamagecurverows*timestep,timestep)
    outcurve[:,1] = np.sum(outmat,axis=0).T
    return outcurve
    
def splitcurves(dataframe,shapecols,timestep,splittingcolumn,splitcolumnprobname,
                probrowname='fullcondprob',damagerowname='damagecurve'):
    """build a dictionary of damage curves off of the dataframe"""
    outdict = {}
    for i in dataframe[splittingcolumn].unique():
        newframe = dataframe.loc[dataframe[splittingcolumn]==i]
        newframe.loc[:,probrowname] = newframe[probrowname] / newframe[splitcolumnprobname]
#         print i
#         print newframe[probrowname].sum()
        outcurve = curvebuilder(newframe.reset_index(drop=True),shapecols,
                                timestep,probrowname,damagerowname)
        outdict.update({i:outcurve})
    return outdict

def drawoutcome(condprobdataframe,time,probrow='fullcondprob',damagerow='damagecurve',roomdamagemetric='roomorigdam'):
    """Draw a random damage outcome from the dataframe given a time
    
       condprobdataframe::dataframe containing conditional probabilities and damagecurves
       time:: time in seconds.
       
       returns: tuple containing (damagevalue, room of origin damage value)
    """
    outcomerow = condprobdataframe.sample(weights=condprobdataframe[probrow])
#     print outcomerow
#     print outcomerow.damagecurve.iloc[0][:,0]
    outcome = outcomerow[damagerow].iloc[0][outcomerow[damagerow].iloc[0][:,0] <= time,1].max()
#     print outcome
#     print float(outcomerow.roomorigdam)
    return (outcome,float(outcomerow[roomdamagemetric]))

def fastdrawgen(condprobdataframe,ndamagecurverows,timestep=10,probrow='fullcondprob',damagerow='damagecurve',roomdamagemetric='roomorigdam'):
    weights = condprobdataframe[probrow]
    indexes = condprobdataframe.index
    roomorigdam = condprobdataframe[roomdamagemetric]
    outmat = np.zeros((len(condprobdataframe)+1,ndamagecurverows))
#     print outmat.shape
    for row in condprobdataframe.itertuples():
#         print row.Index
        outmat[row.Index,:] = getattr(row,damagerow)[:ndamagecurverows,1]
    outmat[-1,:] = range(0,ndamagecurverows*timestep,timestep)
    
    def drawout(time):
        colind = np.max(np.where(outmat[-1,:] <= time))
        rowchoice = np.random.choice(indexes,p=weights)
        outcome = outmat[rowchoice,colind]
        origdam = roomorigdam[rowchoice]
        return(outcome,origdam)
    return drawout

def responseanalyzer(nrecords,resptimes,condprobdataframe,
                     ndamagecurverows,timestep=10,probrow='fullcondprob',
                     damagerow='damagecurve',roomdamagemetric='roomorigdam'):
    drawfunc = fastdrawgen(condprobdataframe,ndamagecurverows,timestep,probrow,damagerow,roomdamagemetric)
    respsens = []
    for n in nrecords:
        for i in resptimes:
            room = 0
            bldg = 0
            for j in range(n):
                damage, roomorig = drawfunc(i)
                if damage > roomorig:
                    bldg += 1
                if damage <= roomorig:
                    room += 1
            outdict = {'nrecords':n,'responsetime':i,'room':room,'bldg':bldg}
            respsens.append(outdict)
    return(pd.DataFrame(respsens))

def exactresponsecalculator(resptimes,condprobdataframe, probrow='fullcondprob',
                            damagerow='damagecurve', roomdamagemetric='roomorigdam'):
    exactsol = []
    for i in resptimes:
        a=0
        for row in condprobdataframe.itertuples():
            if np.any(getattr(row,damagerow)[getattr(row,damagerow)[:,0] <= i,1] > getattr(row,roomdamagemetric)):
                a+=getattr(row,probrow)
        exactsol.append({'bldg':np.nan,
                         'nrecords':'Exact',
                         'responsetime':i,
                         'room':np.nan,
                         'normalizedroom':1-a,
                         'normalizedbldg':a})
    return exactsol

def main(filesdirectory,searchterm,ndamagecurverows=360,timestep=10,probrowname='condprob',damagerowname='rawdamagecurve'):
    outframe = pd.DataFrame()
    inputlist = glob.glob(''.join((filesdirectory,'/',searchterm)))
    for item in inputlist:
        paramstring = '-'.join(os.path.basename(item).split('-')[:-1])
        relevantname = '-'.join([paramstring]+['ecurve.p'])
        inframe = pd.read_pickle(item)
        inframe.reset_index(drop=True)
        eframe = pd.DataFrame(curvebuilder(inframe,ndamagecurverows,
                                             timestep,probrowname,damagerowname),
                               columns=('time','damage'))
        eframe['params'] = paramstring
        outframe = outframe.append(eframe)
    outframe.to_csv('/'.join((filesdirectory,'ecurvecollection.csv')))

if __name__ == "__main__":
    repo_dir = subprocess.Popen(['git', 'rev-parse', '--show-toplevel'], stdout=subprocess.PIPE).communicate()[0].rstrip()
    os.chdir(repo_dir)
    if len(sys.argv) >= 2:
        filesdirectory = os.path.abspath(sys.argv[1])
        searchterm = '*wprobs.p'
    if len(sys.argv) == 3:
        searchterm = sys.argv[3]
    if len(sys.argv) > 3:
        ndamagecurverows = sys.arv[4]
        timestep = sys.argv[5]
        probrowname = sys.argv[6]
        damagerowname = sys.argv[7]
    if len(sys.argv) > 7:
        raise Exception("too many arguments, should be a maximum of 2")
    if len(sys.argv) == 1: #default values
        filesdirectory = os.path.abspath('./outputs/damagecurves/')
        searchterm = '*wprobs.p'
    main(filesdirectory,searchterm)
