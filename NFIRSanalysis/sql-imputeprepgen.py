import yaml
import inspect

def generate_update_statement(tablename,fieldname,listofvals=None,invert=None,
                              newvalue=None):
    """generate a SQL update statement that maps variables to lowest included value"""
    if newvalue:
        firstvalue = newvalue
    else:
        firstvalue = sorted(listofvals)[0]
    args = {'tablename':tablename, 'fieldname':fieldname,
            'firstvalue':firstvalue}
    if not listofvals:
        formatstring = inspect.cleandoc(#
        """UPDATE {tablename} SET {fieldname} = {firstvalue};""".format(**args))
        return formatstring
    args['listofvals'] = "','".join(str(i) for i in listofvals)
    if not invert:
        formatstring = inspect.cleandoc(#
        """UPDATE {tablename} SET {fieldname} = '{firstvalue}' 
        WHERE {fieldname} IN ('{listofvals}');""".format(**args))
    else:
        formatstring = inspect.cleandoc(#
        """UPDATE {tablename} SET {fieldname} = '{firstvalue}' 
        WHERE {fieldname} NOT IN ('{listofvals}');""".format(**args))
    return formatstring

def generate_alter_statement(tablename,fieldname,datatype):
    """generate an alter table statement with specified params"""
    args = {'tablename':tablename,'fieldname':fieldname,
            'datatype':datatype}
    formatstring = inspect.cleandoc(#
    """ALTER TABLE {tablename} ADD COLUMN {fieldname} {datatype};""".format(**args))
    return formatstring


aoo_mapping = yaml.load(open('aoo-ifi-hs-mapping.yaml'))
operate_table = aoo_mapping.pop(0)
newcol_operations = yaml.load(open('sql-imputeprepmap.yaml'))
sqloutput = []
sqloutput.append('BEGIN;')
for operation in newcol_operations:
    fieldname = operation['fieldname']
    datatype = operation['datatype']
    functionmap = operation['functionmap']
    valuemap = operation['map']
    sqloutput.append(generate_alter_statement(operate_table,fieldname,datatype))
    if functionmap:
        sqloutput.append(generate_update_statement(operate_table,fieldname,
                         newvalue = functionmap))
    if valuemap:
        for newval,oldvals in valuemap.items():
            out = generate_update_statement(operate_table,fieldname,
                                            oldvals,newvalue=newval)
            sqloutput.append(out)
sqloutput.append('COMMIT;')
with open('imputepreparation.sql','wb') as f:
    f.writelines("{}\n".format(outputline) for outputline in sqloutput)
