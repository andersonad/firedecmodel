import subprocess
import sys
import os
import imp
from pint import UnitRegistry

def main(configlocation):
    damagecalculator = imp.load_source('damagecalculator',
                                 './CFASTdamagemodel/damagecalculator.py')
    condprobconstruct = imp.load_source('condprobconstruct',
                                 './NFIRSanalysis/cond-prob-construct.py')
    damagecalculator.main(configlocation,'damagecalculator')
    condprobconstruct.main(configlocation,'cond-prob-construct')

if __name__ == "__main__":
    repo_dir = subprocess.Popen(['git', 'rev-parse', '--show-toplevel'], stdout=subprocess.PIPE).communicate()[0].rstrip()
    os.chdir(repo_dir)
    if len(sys.argv) == 2:
        configlocation = os.path.abspath(sys.argv[1])
    elif len(sys.argv) > 2:
        raise Exceptions("Too many system arguments submitted to script")
    else:
        configlocation = os.path.abspath('./modelconfig.yaml')
    main(configlocation)
