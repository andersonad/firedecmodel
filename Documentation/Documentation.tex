\documentclass[letterpaper,12pt]{article}
\usepackage[numbers, sort&compress]{natbib}
\usepackage{graphicx}
\usepackage[margin=1in]{geometry}
\usepackage{enumitem}
\usepackage{multirow}
\usepackage{subcaption}
\usepackage{booktabs}
\usepackage{pdflscape}
\usepackage[hidelinks]{hyperref}
\usepackage[vertfit]{breakurl}
\usepackage{amsmath}
\usepackage{microtype}

\begin{document}
\title{Documentation of U.S. Fire Problem Decision Analysis Model}
\author{Austin Anderson, Ofodike A. Ezekoye, Craig Weinschenk, and Whosoever will}
\date{\today}
\maketitle

\section{Introduction}
This document catalogues the various assumptions involved in building a data structure representing a probabilistic loss model for fires in the United States that is based off of ensemble runs from a physical fire model coupled to national fire incident data. Thus far, the output of this model is a pickled pandas dataframe containing a probability tree with outcomes that is amenable to manipulation in decision models.

\section{Requirements}
Running this model successfully depends upon a number of resources and packages which are assumed to be available to the user. Table~\ref{tab:packagelisting} lists these requirements.

\subsection*{OS}
This model uses a number of shellscripts (.sh extension) to perform various convenience tasks, including parallel execution of constructed CFAST models. This restricts the OS in a rough sense to any platform that supports the following:

GNU parallel, bash scripts

Many common linux distributions appear to support gnu parallel, and these scripts were developed using Ubuntu 14.04. Most ``high-performance'' computing clusters one would run computations from remotely will be carrying some variant of Linux. Windows and MacOS users are left to determine their own methods of running many instances of CFAST jobs in parallel. As a simple workaround, consider, for example, implementing a RabbitMQ server/script whose consumers execute CFAST runs populated by a queue script that builds the list of CFAST runs for execution. See \url{https://www.rabbitmq.com/tutorials/tutorial-two-python.html} for more information on this approach.

\subsection*{Python packages}
\begin{itemize}
  \item Numpy -- basic scientific computing package for python
  \item Pandas -- access to dataframe structures
  \item rdp -- Short for ``Ramer-Douglas-Peucker'' algorithm, this package is used to simplify experimental heat release rate curves for inclusion in CFAST models.
  \item pint -- A package that makes unit conversions in python more accessible and ``easier'' (more consistent) to work with.
  \item tqdm -- It puts a progress bar on for loops, useful for lengthy iterative processes for monitoring progress.
\end{itemize}

\subsection*{Database software and availability}
This model depends upon the existence of a Postgres 9.3 or higher database that contains tables constructed from the National Fire Incident Reporting System (NFIRS) public data release files (PDRs). This data, at present, is available from the United States Fire Administration (USFA) upon request, and will be mailed to the user on CDs.

The user must also have write access to some portion of this database, as certain operations involve the generation and manipulation of PDR tables.

Another source of data that contributes to this model is the American Housing Survey's (AHS) Public Use Files (PUFs). At present these files are available from the U.S. Census bureau's website at
\url{http://www.census.gov/programs-surveys/ahs/data/2013/ahs-2013-public-use-file--puf-/2013-ahs-metropolitan-puf-microdata.html}

While this dataset is not tremendously large, it is formatted for use in a relational data structure, so it is recommended to place it in a database as well for analysis. Note that model execution does not depend on the presence of this database, it is there for analysts dealing with CFAST models.

\subsection*{CFAST}
The Consolidated Model of Fire and Smoke Transport (CFAST) is available from \url{https://github.com/firemodels/cfast}. Linux and MacOS users will need to check out and compile the code on their own. Windows users may download the executable files available on the releases page. This is the fire model presently chosen for running scenarios.

\section{Model modules}
At present, the process of constructing the end product is compartmentalized into a number of what might be considered modules, sorted by directory. Here these modules and their contingent files will be outlined and the pertinent workflow and output discussed. Discussion will proceed in the order outlined in Figure~\ref{fig:moduleworkflow}

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.8\textwidth]{./Figures/ModuleRelations}
  \caption{Module workflow relations. Dashed lines indicate support data or functions that help a primary module.}
  \label{fig:moduleworkflow}
\end{figure}

\subsection{itemhrrprocessing}
This submodule contains a python file with helper functions, hrr_reader.py, for reading and processing experimental HRR data for use by the psradius.py function within the formprocessing module. Additionally, the itemhrr folder contains the HRR curves for every item used by the rest of the process.

Note: HRR curves must be named according to the items to which they correspond in the semantic process. Here, the survey forms for room layouts dictated which items were in a given room, and thus these HRR curves match the items outlined on those forms. This naming convention is followed throughout the process for room items.

Todo: Add a .bib file containing the references for each item's HRR.

\subsection{Furniturelayout}
The furniturelayout module contains the results of a room layout survey conducted in April 2016, as well as a number of python scripts designed to aid in processing of the data from the forms obtained. It also contains a file, psradius.py, that calculates expected secondary ignition of items in room layouts based on the survey results.

\subsubsection{Room Layout Survey and support scripts}
The printable forms folder contains the survey forms administered to participants in the surveys. The FurnitureLayoutData folder and contains the raw image data from survey participant forms as well as the structured item-to-item centerpoint distance data. More detail regarding this data can be found in output-format-notes.txt

calcdist.py and distcalc.csv are a support script and file used in conjunction with webplotdigitizer to digitize the contents of the raw image data.
1. following the column ordering in output-format-notes.txt, select the center of the relevant item using webplotdigitizer.
2. Copy/paste the points into distcalc.csv
3. run calcdist.py, with the ``wut'' variable pointing to the appropriate form type's directory.

\subsubsection{psradius.py}
psradius.py calculates time-to-ignition for all items in a given room layout, given one item is ignited first. The ``datasources'' variable points to the directories containing the results of the surveys, and requires the square item-to-item distance matrix .csv files generated from the room layout survey support scripts to function.

furniturereff.csv contains the ``effective'' radius of the furniture items, approximated as follows:
\[P_item = 2\pi r_eff\]
where $P_item$ is the perimeter of the furniture item and the effective radius is the radius of a circle of equivalent perimeter.

The rest of the geometries relevant to the problem are presented in Figure~\ref{fig:PSReffective}. The relevant geometries are as follows:
\begin{itemize}
  \item $d_{\text{QN}}$ is the center-to-center distance from Q to N, this is a symmetric relation.
  \item $r_{\text{eff,Q}}$ is the ``effective radius'' of Q, calculated as discussed above.
  \item $r_{\text{QW}}$ is the radius used for the point-source radiation calculation, assuming heat release from Q reaching W. $r_{\text{DQ}}$ would be the same, assuming that D was burning and Q was exposed.
\end{itemize}

Note the following: $r_{\text{DQ}} = d_{\text{DQ}} - r_{\text{eff,Q}}$ thus, the only distances of relevance are the effective radius of items and their center-to-center distances. The rest are derived quantities.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.95\textwidth]{./Figures/PSReffective}
  \caption{Approximation of room layout into representative geometry. The items in the form (left) are approximated as circles of equivalent perimeter (middle). Point-source radiation calculations are then performed using relevant geometric features portrayed (right).}
  \label{fig:PSReffective}
\end{figure}

psradius.py outputs three items, outputdic.p is a nested dictionary containing the roomnames of the survey room layouts, which then contain the set of items in those rooms, each item of which then is mapped to an $n$ by $m$ array of items, where $n$ is the number of room layout survey samples containing that item, and $m$ is the number of items in that particular room layout, with positioning corresponding to that outlined in output-format-notes.txt under FurnitureLayoutData/. The values in this array correspond to the amount of time before the heat release rate from the item is high enough to cause a heat flux equal to or greater than the specified ignitability value in the script.

hrrdic.p is a dictionary mapping the items in the room to their simplified heat release curves. Heat release curves are simplified through application of the RDP algorithm to the experimental data, to bring the total amount of HRR points in the curve below the threshold required by CFAST.

refdic.p is a dictionary mapping the survey room layouts to the index numbers of the items found in them according to furniturereff.csv. these index values further map to a list containing a given item and its effective radius.

These three output values are used by the modelbuilding script to construct the CFAST input files for simulation.

\subsection{CFASTmodelbuild}
This module contains the workhorse script CFASTbuilder.py, which constructs all of the CFAST runs using the base CFAST models contained in homeplans, as well as the output from psradius.py and a given ignitability value.

Note that users need to specify the following values in the script if they disagree with the defaults
\begin{itemize}
  \item outputdir -- specify the directory where the constructed cfast model files are to be output
  \item cfastmodellist -- specify the directory where the base cfast models are located
  \item ignitability -- specify the ignitability to be used for heat flux ignition conditions of secondary items and room, in W/m\^2
  \item roommap -- a dictionary containing the mapping between the names of the room survey layouts, and the base names of these rooms in the base cfast models
\end{itemize}

This script will output, by default, 5167 cfast input files into a ``CFASTruns'' directory created within the CFASTmodelbuild directory. This directory \textbf{is not there by default and must be made}.

listbuilder.sh and filerunner.sh are scripts that aid in running the ensemble of CFAST models in parallel on a machine. listbuilder constructs the list of input files with proper directory structure, filerunner executes GNU parallel with CFAST and the input file list generated from listbuilder.sh.

Note: Users need to point filerunner.sh's CFAST statement to a valid CFAST executable. Depending on CPU speed and ventilation conditions, 5167 simulations may take between 30 minutes and 30 hours to run.

\section{CFASTdamagemodel}
This module contains the workhouse script damagecalculator.py, which calculates the ``damage curves'' using floor target heat flux data from the ensemble of CFAST runs. See Duhamel.Damage.Integral.ipynb for a brief treatment of how these damage curves are calculated.

The brief summary is as follows: A semi-infinite 1-D approximation of wall temperature is considered for a wooden sample being exposed to the heat flux curves supplied by the CFAST run output. This wall temperature is then used to calculate an approximation of the amount of pyrolysis occurring in the wooden sample assuming a simple Arrhenius reaction form. Separately, the total amount of pyrolysis required to cause ignition in a wood sample with equivalent properties is calculated assuming exposure to a constant heat flux boundary condition where the heat flux is the ignitability criterion, here 15 kW/m\^2. The total pyrolysis calculated by the heat release rate curves from the CFAST runs is then normalized by the total pyrolysis required to ignite the sample in ``flashover'' conditions.

This script has many possible knobs to consider, listed as follows:
\begin{itemize}
  \item modelfiledirectory -- the location of the base CFAST model files.
  \item inputfilelist -- the location of the cfast run output.
  \item Tign -- the surface temperature of ignition for the comparative wood sample.
  \item krhoc -- $k\rho c$, the product of the thermal conductivity, density, and specific heat capacity.
  \item qdub -- the heat flux associated with flashover conditions.
  \item To -- the initial temperature of the sample, typically room temperature.
  \item E -- The activation energy associated with the arrhenius approximation of the pyrolysis reaction.
  \item R -- The universal gas constant.
\end{itemize}

The output of the script is damagecurveoutput.p, which is a pandas dataframe where each row contains the relevant base cfast model, area of origin, first item ignited, sample layout number, and the damage curve associated with that model.

damage_visualization.ipynb is just an ipython notebook that helps visualize damagecurveoutput.p, and remove_output.py is just a helper script that removes output from .ipynb if a wrongly placed for-loop causes the website to lock up.

\subsection{NFIRSanalysis}
This module has a large number of scripts involved, as well as some definition files, demarcated by the .yaml extension. If one wants to change the underlying categorizations or assumptions in the analysis, edit the yaml files and regenerate the SQL code.

NFIRSanalysissetup.sql contains sql code that generates a table that is a subset of NFIRS data containing building fires filtered by a property use code corresponding to residential structure fires.

sql-confineassume.py generates sql code that reassigns fields in the NFIRS table according to the mappings described in sql-confineprepmap.yaml. These are ``strong'' assumptions about areas or origin, items first ignited, or heat sources ascribed to confined fire incidents. For example, confined fire type ``118,'' defined as a ``Trash or rubbish fire in a structure, with no flame damage to structure or its contents,'' is assumed to have item first ignited ``96,'' defined as ``Rubbish, trash, waste.''

sql-AoO-IFImapgen.py generates sql code that aggregates NFIRS categories together according to the needs of the analysis, as defined in aoo-ifi-hs-mapping.yaml. Categories listed under include are retained as individual fields, while categories listed under exclude are converted to ``Other.'' This allows testing of different assumptions or inclusion of more categories later down the line should the model expand in scope to include them.

sql-imputeprepgen.py generate sql code that produces the columns relevant to the multiple imputation procedure used to apportion the unknowns in the data.

impute-script.R is an R script that utilizes the Amelia library to perform multiple imputation of the dataset following an expectation-minimization boostrapping algorithm. It then writes this multiply imputed dataset back to the host database.

cond-prob-construct.py is the workhorse script of this module. It develops and fleshes out the branches of the probability tree for damagecurveoutput.p. Relative CFAST model/home selection probabilities contained in homeconfigprobs.yaml are presently taken from the analysis conducted in AHSanalysis, which is not automated and uses the American Housing Survey's (AHS) public use files (PUF). 


\end{document}
