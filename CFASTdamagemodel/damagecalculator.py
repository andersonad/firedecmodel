import imp
import subprocess
import sys
import yaml
import pdb
import numpy as np
import pandas as pd
import glob
import os
from tqdm import tqdm
from pint import UnitRegistry

def twbuilder(To,krhoc,qdub,start,stop,stepsize):
    """compute wall temperature profile over time for flashover condition exposure
    
    Please make sure start stop and stepsize all play nice together.
    """
    if stop < stepsize:
        raise Exception("stepsize is larger than stop value! Increase the resolution.")
    stop = stepsize*round(stop/stepsize) + stepsize
    output = np.empty((len(np.arange(start,stop,stepsize)),2))
    for ind,i in enumerate(np.arange(start,stop,stepsize)):
        output[ind,0] = i
        output[ind,1] = To+(4/(np.pi*krhoc))**(1./2.)*qdub*np.sqrt(i)
    return output

def pyrointegral(E,R,time,walltemp):
    """calculate un-normalized total amount of pyrolysis in target"""
    pyrovalue = np.exp(-(E/(R*walltemp)))
    totvalue  = np.trapz(pyrovalue,time)
    return totvalue

def Twallsolver(times,heatfluxes,initialtemp,krhoc):
    """Build and return wall temperature values for a given input of heat flux values
    
       Note: returns the wall temperature for each given time step using a Duhamel 1-D semi-infinite solid approximation
       heatfluxes::numpy array
       initialtemp::float
       times::numpy array
    """
    Nsteps = len(times)
    timestep = times[1]-times[0] #this fails for irregular timesteps, we expect timestep to be constant
    Nvec = np.asarray([Nsteps-i for i in np.arange(Nsteps)])
    delq = np.append(heatfluxes,0)
    delq = delq-np.roll(delq,1)
    delq = np.delete(delq,len(delq)-1)
    output = np.empty(Nsteps)
    for ind in np.arange(len(delq)):
        output[ind] = initialtemp+(4/(np.pi*krhoc))**(1./2.)*np.inner(delq[0:ind+1],np.sqrt(timestep*Nvec[Nsteps-ind-1:Nsteps]))
    return output

def damagecurvecalc(time,Twall,E,R,flashoverdamage):
    """Calculate the relative damage curve for a given target
    
        time - array of times
        Twall - array of target surface temperatures
        E - Arrhenius-style activation energy of target in #J/mol (see Agrawal 1985 "On the use of the arrhenius equation
        to describe cellulose and wood pyrolysis
        R - universal gas constant #J/mol
        flashoverdamage - total pyrolysis computed for wood subjected to flashover heat flux until ignition)
    """
    if len(time) != len(Twall):
        raise Exception("Lengths of time and Twall vectors unequal")
    damageoutput = np.empty(len(Twall))
    for ind in np.arange(len(damageoutput)):
        damageoutput[ind] = pyrointegral(E,R,time[:ind+1],Twall[:ind+1])
    damagecurve = np.empty(len(damageoutput))
    for ind in np.arange(len(damagecurve)):
        damagecurve[ind] = min(1,damageoutput[ind]/flashoverdamage)
    return damagecurve


def strucdamagecalc(inputfile,modelfiledirectory,flashoverdamage,krhoc,E,R,initialtemp,ce):
    """calculates a structural damage curve based on a model input file
    
    inputfile::cfast *_w.csv file, preferably following a hierarchical naming convention
               with base model name in the first position
    modelfile:: corresponding directory where base model files are stored.
    flashoverdamage:: value of integral of pyrolysis reaction over target wood of choice when exposed 
                  to designated flashover heat flux
    initialtemp::initial temperature of target (K)
    krhoc:: value of thermal conductivity k, density rho, and specific heat capacity c (kJ^2 m^-4 K^-2 s^-1)
    E :: activation energy of wood pyrolysis arrhenius fit (J/mol)
    R :: universal gas constant (J/mol)

    output: numpy array containing a time column and a total damage column.
    """
    ureg = UnitRegistry()
    Q_ = ureg.Quantity
    #Room size calcs
    # print "krhoc = {}, E = {}, R = {} To = {}".format(krhoc,E,R,initialtemp)
    infile = os.path.basename(inputfile).split('.')[0]
    room = infile.split('-')[1]
    basemodel = infile.split('-')[0]+'.in'
    test = ce.cfastExtract(os.path.abspath('/'.join((modelfiledirectory,basemodel))))
    roomsizes = {a[0]:float(a[1])*float(a[2]) for a in test.rooms.values()}
    roomtot = sum(roomsizes.values())
    homesize = Q_(int(infile.split('_')[1]),'foot**2').to('meter**2').magnitude
    #damage calcs
    pattern = r'TRGFLXT_'
    raw_data = pd.read_csv(inputfile,skiprows=[1,2,3])
    tfluxcols = raw_data.loc[:,raw_data.columns.str.contains(pattern)]
    time = raw_data['Time'].as_matrix()
    name_data = pd.read_csv(inputfile,nrows=2)
    tflux_colnames = name_data.loc[:,raw_data.columns.str.contains(pattern)]
    targtrans = {a:b.replace("floortarget","") for (a,b) in zip(tflux_colnames.columns,tflux_colnames.iloc[1,:])}
    damageroomcurves = tfluxcols.copy()
    for name, i in tfluxcols.iteritems():
        damageroomcurves[name] = damagecurvecalc(time,Twallsolver(time,i,initialtemp,krhoc),E,R,flashoverdamage)*roomsizes[targtrans[name]]
    alldamage = damageroomcurves.sum(axis=1)
    #calculate percentage of damage corresponding to room of origin area + 0.01 meters
    roomorigdam = (roomsizes[room]+0.01) / sum(roomsizes.values())
#     if np.any(alldamage > roomsizes[room]+0.01):
#         print 5
    totdamage = alldamage / sum(roomsizes.values())
    # print np.column_stack((time[:10],totdamage[:10],))
    return (roomorigdam, np.column_stack((time,totdamage,)), roomsizes[room], np.column_stack((time,alldamage)))


def main(configloc,configkey):
    
    configs = yaml.load(open(configloc))
    configuse = configs[configkey]
    ce = imp.load_source('cfastExtract','./CFASTdamagemodel/cfastExtract.py')
    outputlocation = os.path.abspath(configuse['outputlocation'])
    modelfiledirectory = os.path.abspath(configuse['modelfiledirectory'])
    inputfilelist = glob.glob(os.path.abspath(configuse['inputfilelist']))
    Tign = configuse['Tign'] #K Oak veneer plywood
    krhoc = configuse['krhoc'] #kJ^2/(m^4*K^2*s)
    qdub = configuse['qdub'] #kW/m^2
    To = configuse['To'] #K
    #print To
    E= configuse['E'] #J/mol
    R = configuse['R'] #J/(mol*K)



    #calculate ignition time for pyrolysis sample target using
    #Duhamel semi-infinite approximation
    tignfinder = (Tign-To)**2*(np.pi/4)*krhoc/(qdub)**2
    # print tignfinder

    #calculate wall temperature profile for sample target
    twflash = twbuilder(To,krhoc,qdub,0,tignfinder,tignfinder/100)

    #calculate total pyrolysis using "flashover" wall temperature profile
    #Note that this 
    flashdam = pyrointegral(E,R,twflash[:,0],twflash[:,1])
    # print flashdam

    #construct the pandas dataframe housing the data
    d = []
    i = 0
    for filename in inputfilelist:
    #     print filename
        infile = os.path.basename(filename).split('.')[0]
    #     print infile
        parsefile = infile.split('-')
        roomorigdamage, damagetot, rawroomorigdamage, rawdamagetot = strucdamagecalc(filename,modelfiledirectory,flashdam,krhoc,E,R,To,ce)
        d.append({'modelname':parsefile[0],'roomname':parsefile[1],'roomtype':parsefile[2],'itemname':parsefile[3],
                  'samplenumber':parsefile[4].split('_')[0], 'damagecurve':damagetot, 'roomorigdam':roomorigdamage,
                  'rawdamagecurve':rawdamagetot, 'rawroomorigdamage':rawroomorigdamage})
        i+=1
        
    print i
    damageoutput = pd.DataFrame(d)
    damageoutput.to_pickle(outputlocation)

if __name__ == "__main__":
    repo_dir = subprocess.Popen(['git', 'rev-parse', '--show-toplevel'], stdout=subprocess.PIPE).communicate()[0].rstrip()
    os.chdir(repo_dir)
    configloc = './modelconfig.yaml'
    if len(sys.argv) == 2:
        configloc = sys.argv[1]
    filename = os.path.splitext(os.path.basename(__file__))[0]
    main(configloc,filename) #default relative path to configuration file
