BEGIN;
ALTER TABLE onefamdetfiresproc ADD COLUMN xhour numeric;
UPDATE onefamdetfiresproc SET xhour = sin(2*pi()*(date_part('hour',alarm)::numeric+date_part('minute',alarm)::numeric/60)/24);
ALTER TABLE onefamdetfiresproc ADD COLUMN yhour numeric;
UPDATE onefamdetfiresproc SET yhour = cos(2*pi()*(date_part('hour',alarm)::numeric+date_part('minute',alarm)::numeric/60)/24);
ALTER TABLE onefamdetfiresproc ADD COLUMN season text;
UPDATE onefamdetfiresproc SET season = date_part('month',alarm);
UPDATE onefamdetfiresproc SET season = 'spring' 
WHERE season IN ('3','4','5');
UPDATE onefamdetfiresproc SET season = 'summer' 
WHERE season IN ('6','7','8');
UPDATE onefamdetfiresproc SET season = 'winter' 
WHERE season IN ('12','1','2');
UPDATE onefamdetfiresproc SET season = 'fall' 
WHERE season IN ('9','10','11');
ALTER TABLE onefamdetfiresproc ADD COLUMN climate_reg text;
UPDATE onefamdetfiresproc SET climate_reg = state;
UPDATE onefamdetfiresproc SET climate_reg = 'northeast' 
WHERE climate_reg IN ('CT','DE','ME','MD','MA','NH','NJ','NY','PA','RI','VT','DC');
UPDATE onefamdetfiresproc SET climate_reg = 'central' 
WHERE climate_reg IN ('IL','IN','KY','MO','OH','TN','WV');
UPDATE onefamdetfiresproc SET climate_reg = 'west' 
WHERE climate_reg IN ('CA','NV');
UPDATE onefamdetfiresproc SET climate_reg = 'eastnorthcentral' 
WHERE climate_reg IN ('IA','MI','MN','WI');
UPDATE onefamdetfiresproc SET climate_reg = 'southeast' 
WHERE climate_reg IN ('AL','FL','GA','NC','SC','VA','PR');
UPDATE onefamdetfiresproc SET climate_reg = 'northwest' 
WHERE climate_reg IN ('ID','OR','WA','AK');
UPDATE onefamdetfiresproc SET climate_reg = 'westnorthcentral' 
WHERE climate_reg IN ('MT','NE','ND','SD','WY');
UPDATE onefamdetfiresproc SET climate_reg = 'south' 
WHERE climate_reg IN ('AR','KS','LA','MS','OK','TX');
UPDATE onefamdetfiresproc SET climate_reg = 'southwest' 
WHERE climate_reg IN ('AZ','CO','NM','UT','HI','MP','NA');
ALTER TABLE onefamdetfiresproc ADD COLUMN fireid serial unique;
COMMIT;
