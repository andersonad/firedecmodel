from __future__ import division
from scipy.spatial.distance import cdist
import numpy as np
import glob
import os
import re
from itertools import chain

def missing_elements(L):
    start, end = L[0], L[-1]
    return sorted(set(range(start, end + 1)).difference(L))
wut = glob.glob('FurnitureLayoutData\MasterBedroom\*.csv')
wut2 = [os.path.splitext(os.path.basename(x))[0] for x in wut]
wut3 = [re.findall('\d+', x) for x in wut2]
wut4 = list(chain.from_iterable(wut3))
wut5 = sorted([int(x) for x in wut4])
if wut5:
    missing_ele = missing_elements(wut5)
else: 
    missing_ele = []

if not missing_ele:
    filenum = len(wut5)+1
else:
    filenum = missing_ele[0]

filename = os.path.abspath("FurnitureLayoutData\MasterBedroom\\")+"""\\sub-{}.csv""".format(filenum)
print filename


test = np.loadtxt('distcalc.csv',delimiter=',')
test[np.where( test < 0)] = np.nan
dista = cdist(test,test)
#np.savetxt(filename,dista,delimiter=',')
