import subprocess
import sys
import yaml
from sqlalchemy import create_engine
import pandas as pd
import inspect
import pdb
import math
import operator
import getpass
import os

def removedigits(stringhere):
    return ''.join(i for i in stringhere if not i.isdigit())

def probnormalize(probvector):
    """returns the probability vector with values normalized to sum to 1"""
    return probvector / probvector.sum()

def fieldretrieve(yamldic,fieldname):
    """retrieves a given fieldname's 'include' dictionary values"""
    for item in yamldic:
        try: 
            if (item['fieldname'] == fieldname):
                return item.get('include')
        except:
            pass
    raise Exception('Unable to find fieldname in yamldictionary')

def really_safe_normalise_in_place(d):
    """normalizes the values in a dictionary in-place"""
    if not d:
        return None
    factor=1.0/math.fsum(d.itervalues())
    for k in d:
        d[k] = d[k]*factor
    key_for_max = max(d.iteritems(), key=operator.itemgetter(1))[0]
    diff = 1.0 - math.fsum(d.itervalues())
    #print "discrepancy = " + str(diff)
    d[key_for_max] += diff

def branchnormalizer(dataframe,columnname,refdict,lookahead=None):
    """normalize the values within a branch and save them to a refdict

       note: lookahead should also be a list containing the column name and 
             reference dictionary call for the next branch beyond the present.
             This examination is necessary in order to catch
             null/empty branches. For example, if I have two roomtypes,
             but one of the room types has no items within it that match
             the item first ignited category, I need to prune that whole
             roomtype before normalizing or I will arbitrarily reduce the
             contribution of that AoO/IFI combination by, in this example,
             half
    """
    normdict = {}
    seenit = set()
    retdict = {}
    lookdict = {}
   # print refdict
    for names in dataframe[columnname].unique():
        namesnonum = removedigits(names)
        if namesnonum in refdict.keys():
            if lookahead:
                nextframe = dataframe.loc[dataframe[columnname] == names]
                lookdict = branchnormalizer(nextframe,lookahead[0],lookahead[1])
                if lookdict:
                    normdict.update({names:refdict[namesnonum]})
                else:
                    print "{} did not contain leaves, it will be ignored".format(names)
            else: 
                normdict.update({names:refdict[namesnonum]})
    really_safe_normalise_in_place(normdict)
    for key,value in normdict.items():
        if removedigits(key) not in seenit:
            seenit.add(removedigits(key))
            retdict[removedigits(key)] = value
    return retdict    

def main(configlocation,configkey):

    configs = yaml.load(open(configlocation))
    configuse = configs[configkey]

    outputfilename=configuse['outputfilename']
    try: 
        print "cached dataframe aooifiprobs.p present, using in lieu of a query"
        aooifidf = pd.read_pickle(configuse['aooifidf'])
    except:
        print "aooifiprobs.p does not exist, querying database and generating"
        aoo_mapping = yaml.load(open(configuse['aoomappingloc']))
        outputtable = "".join(aoo_mapping.pop(0)+'imp')
        del aoo_mapping[0]
        fieldnames = []
        for item in aoo_mapping:
            fieldnames.append(item.get('fieldname'))
        #we foolishly assume the first 2 fieldnames are those corresponding to
        #the area of origin and item first ignited respectively.
        #we also overwrite the heat source field with the outputtable name so the
        #object unpacks correctly
        fieldnames[2] = outputtable
        #build the sql query that returns the probabilities and conditionals
        sqlquery = inspect.cleandoc(#
                """select {0}, {1}, avg(perc) from 
                (select imputation, {0}, {1}, 
                count(*)::numeric / (select count(*) from 
                {2} group by imputation limit 1) as perc from 
                {2} group by imputation, {0}, {1}) as t 
                group by {0}, {1} order by {0}, {1};""".format(*fieldnames)
                )
        #print sqlquery
        connstring = "postgresql://{user}:{password}@{host}:{port}/{dbname}".format(
                    **{'dbname':os.environ['NFIRS_DATABASE_NAME'],
                       'user':os.environ['NFIRS_DATABASE_USER'],
                       'port':os.environ['NFIRS_DATABASE_PORT'],
                       'host':os.environ['NFIRS_DATABASE_HOST'],
                       'password':os.environ['NFIRS_DATABASE_PASSWORD']})
                     
        engine = create_engine(connstring)

        print "Now running SQL query"
        aooifidf = pd.read_sql_query(sqlquery,con=engine)
        print "SQL query complete"
        aooifidf.to_pickle("aooifiprobs.p")
        print "result cached to aooifiprobs.p"

    print aooifidf.avg.sum()

    #create new dataframe with aoo and ifi '00' (other) cases from removed from the output
    aooifidf2 = aooifidf.loc[~(aooifidf.area_orig == '00') & ~(aooifidf.first_ign == '00')]
    print aooifidf2.avg.sum()
    aooifidf2.loc[:,'avg'] = probnormalize(aooifidf2.avg)

    #Now we need the pandas dataframe containing the damageoutputcurves
    damageoutput = pd.read_pickle(configuse['damagecurveloc'])

    #import model config conditional probs
    modelnamecond = yaml.load(open(configuse['homeconfigprobs']))
    really_safe_normalise_in_place(modelnamecond)
    #print modelnamecond

    #import yaml document with ifi and aoo mappings to NFIRS fields
    aooifihsfields = yaml.load(open(configuse['aoomappingloc']))
    aoofields = fieldretrieve(aooifihsfields,'area_orig')
    ififields = fieldretrieve(aooifihsfields,'first_ign')
    # print aoofields
    # print ififields

    for key,value in aoofields.items():
        aooifidf2.loc[aooifidf2.area_orig.isin(value),'area_orig'] = key
    for key,value in ififields.items():
        aooifidf2.loc[aooifidf2.first_ign.isin(value),'first_ign'] = key
        
    #import yaml document with ifi and aoo mappings to room objects and rooms
    allaooifitypemaps = yaml.load(open(configuse['ifiaoo-to-item']))
    aootypemap = allaooifitypemaps[0].get('roomnames')
    ifitypemap = allaooifitypemaps[1].get('roomnames')
    roomtypemap = allaooifitypemaps[2].get('roomtypes')

    inputdictlist = []
    damageoutput['roomitemprob'] = 0

    print "Recursing through aoo-ifi combinations and calculating probabilities"
    #generate a massive for loop again, this time iterating through all tree paths involving a given model, room, and furniture
    for model in damageoutput.modelname.unique():
        print model
        damageoutput.loc[damageoutput.modelname == model, 'modelnameprob'] = modelnamecond[model]
        modelsubset = damageoutput.loc[damageoutput.modelname == model]
        for row in aooifidf2.itertuples():
            branchrowlist = []
            roomdef = branchnormalizer(modelsubset,'roomname',aootypemap[row.area_orig]) #calculate proper roomdef values based on model
            for roomnames in modelsubset.roomname.unique():
                roomnamesubset = modelsubset.loc[modelsubset.roomname == roomnames]
                roomnonum = removedigits(roomnames)
                if roomnonum in roomdef.keys(): 
                    for roomtypenames in roomnamesubset.roomtype.unique():
                        roomtypesubset = roomnamesubset.loc[roomnamesubset.roomtype == roomtypenames]
                        #call subset of loop above you 
                        roomtypedef = branchnormalizer(roomnamesubset,'roomtype',
                                      roomtypemap[roomnonum],
                                      lookahead=['itemname',ifitypemap[roomnonum][row.first_ign]])
                        itemdef = branchnormalizer(roomtypesubset,'itemname',ifitypemap[roomnonum][row.first_ign])
    #                             print itemdef
                        for itemnames in roomtypesubset.itemname.unique():
    #                                 print itemnames
                            itemnamenonum = removedigits(itemnames)
                            if itemnamenonum in itemdef:
                                outputdict = {'model': model, 'roomname':roomnames, 
                                              'roomnameprob': roomdef[roomnonum], 'roomtype':roomtypenames, 
                                              'roomtypeprob': roomtypedef[roomtypenames], 'itemname':itemnames, 
                                              'itemnameprob': itemdef[itemnamenonum], 'aooifiprob': row.avg}
                                branchrowlist.append(outputdict)
            testdatframe = pd.DataFrame(branchrowlist)
            if not testdatframe.empty:
                testdatframe['roomitemprob'] = testdatframe.aooifiprob* \
                    testdatframe.roomnameprob* \
                    testdatframe.roomtypeprob* \
                    testdatframe.itemnameprob
                for row in testdatframe.itertuples():
                    booleanmask = (row.model==damageoutput.modelname) & \
                        (row.roomname == damageoutput.roomname) & \
                        (row.roomtype == damageoutput.roomtype) & \
                        (row.itemname == damageoutput.itemname)
                    damageoutput.loc[booleanmask,'roomitemprob'] = \
                        damageoutput.loc[booleanmask,'roomitemprob'] + \
                        row.roomitemprob

    #generate the samplenumber probabilities based on branch (this is uniform
    #within a branch)
    for model in damageoutput.modelname.unique():
        modelset = damageoutput.loc[damageoutput.modelname == model]
        for room in modelset.roomname.unique():
            roomset = modelset.loc[modelset.roomname == room]
            for roomtypes in roomset.roomtype.unique():
                typeset = roomset.loc[roomset.roomtype == roomtypes]
                for item in typeset.itemname.unique():
                    booleanmask = (model==damageoutput.modelname) & \
                    (room == damageoutput.roomname) & \
                    (roomtypes == damageoutput.roomtype) & \
                    (item == damageoutput.itemname)
                    damageoutput.loc[booleanmask,'samplenumberprob'] = 1./len(damageoutput.loc[booleanmask])

    damageoutput['condprob'] = damageoutput.modelnameprob * \
        damageoutput.roomitemprob * damageoutput.samplenumberprob
    damageoutput.to_pickle(outputfilename)

if __name__ == "__main__":
    repo_dir = subprocess.Popen(['git', 'rev-parse', '--show-toplevel'], stdout=subprocess.PIPE).communicate()[0].rstrip()
    os.chdir(repo_dir)
    filename = os.path.splitext(os.path.basename(__file__))[0]
    main('./modelconfig.yaml',filename)
