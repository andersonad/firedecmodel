import yaml
import re
import shutil
import pdb
import glob
import subprocess
import os
import sys

def absoluteFilePaths(directory):
   for dirpath,_,filenames in os.walk(directory):
       for f in filenames:
           yield os.path.abspath(os.path.join(dirpath, f))

def main(configloc):
    configloc = 'cfastmodelrunparams.yaml'
    inputconfig = yaml.load(open(configloc))
    jobbuilderscriptloc = inputconfig['jobbuilderscriptloc']
    jobbuildererrorfilename = inputconfig['jobbuildererrorfilename']
    CFASTrunsdirectory = inputconfig['CFASTrunsdirectory']
    cfastexeclocation = inputconfig['cfastexeclocation']
    filerunnerscriptloc = inputconfig['filerunnerscriptloc']
    errorrunlogname = inputconfig['errorrunlogname']
    errorfilesdirectory = inputconfig['errorfilesdirectory']
    changefloorscriptloc = inputconfig['changefloorscriptloc']
    runlogname = inputconfig['runlogname']
    njobs = inputconfig['njobs']
    timeout = inputconfig['timeout']


    #locate runs that froze
    frozenruns = glob.glob(os.path.join(CFASTrunsdirectory,'*.kernel*'))
    frozenruns = [os.path.abspath(w.replace('.kernelisrunning','.in')) for w in frozenruns]
    print len(frozenruns)

    errorruns = []
    i=0
    for fname in absoluteFilePaths(CFASTrunsdirectory):    # change directory as needed
        if os.path.isfile(fname):    # make sure it's a file, not a directory entry
            if os.path.splitext(fname)[1] == '.log':
                with open(fname) as f:   # open file
                    for line in f:       # process line by line
                        if re.search("Error|error",line):    # search for string
                            errorruns.append(os.path.abspath(fname))
                            break
    errorruns = [os.path.abspath(w.replace('.log','.in')) for w in errorruns]

    # #copy error files
    for fname in frozenruns:
        shutil.copy(fname,errorfilesdirectory)
    for fname in errorruns:
        shutil.copy(fname,errorfilesdirectory)

    # #run changefloor on error files
    subprocess.call([changefloorscriptloc,'-d',errorfilesdirectory])

    # #run error files
    # #second call to jobbuilder
    subprocess.call([jobbuilderscriptloc, '-t',jobbuildererrorfilename,'-s',
                     errorfilesdirectory,'-e','.in',cfastexeclocation])

    # #second call to filerunner
    filerunnercall = [filerunnerscriptloc,'-l',runlogname,jobbuildererrorfilename]
    if njobs:
        filerunnercall.insert(3,njobs)
        filerunnercall.insert(3,'-j')
    if timeout:
        filerunnercall.insert(3,timeout)
        filerunnercall.insert(3,'-t')
    subprocess.call([str(i) for i in filerunnercall])

if __name__ == "__main__":
    repo_dir = subprocess.Popen(['git', 'rev-parse', '--show-toplevel'], stdout=subprocess.PIPE).communicate()[0].rstrip()
    os.chdir(repo_dir)
    if len(sys.argv) == 2:
        configlocation = os.path.abspath(sys.argv[1])
    elif len(sys.argv) > 2:
        raise Exceptions("Too many system arguments submitted to script")
    else:
        configlocation = os.path.abspath('cfastmodelrunparams.yaml')
    main(configlocation)
