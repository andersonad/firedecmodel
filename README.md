#Python Dependencies
 * numpy 
 * pandas 
 * rdp 
 * pint 
 * tqdm
#GNU Dependencies
 * GNU parallel
 * BASH shellscripts called by python

#Instructions for operating model:
1. Primary model tuning parameters are found in modelconfig.yaml
2. After setting any preferences in the psradius and CFASTbuilder sections of the config file, run CFASTmodelgenerator.py
3. Edit cfastmodelrunparams.yaml and add the location of the cfast executable under 'cfastexeclocation'
4. Run CFASTmodelrunner.py, you may need to time-out certain CFAST jobs that won't complete.
5. Run CFASTmodelerrorcheck.py
6. Copy CFAST runs that ended without errors back to the CFASTruns directory, delete any runs that failed to execute in both cases from the CFASTruns directory.
7. Edit NFIRSanalysis/homeconfigprobs.yaml and ensure that the key names match the base model names and that the probabilistic weightings for them are correct, as outlined in the AHSanalysis directory. **This step is only necessary if using non-default CFAST models and calculating probabilistic weights for their scenarios.**
8. Run postCFASToutputgen.py after setting any preferences in the remainder of the config file.
9. You should now have a damagecurveswprobs.p file under outputs/damagecurves, this file contains a distillation of the damage and probability data from the rest of the process.
10. Perform any relevant analyses desired using this data.

##Optional Steps
These steps are to be performed after step 7. of the preceding instructions, provided that the user wants to make alterations to the probability settings or NFIRS to item mappings.
Note: These steps required a working NFIRS database with a table containing the joins between the basicincident and fireincident tables.

0. Ensure that the user has the following environment variables defined:
    - NFIRS\_DATABASE\_HOST: name of the host for the NFIRS database
    - NFIRS\_DATABASE\_NAME: name of the NFIRS database
    - NFIRS\_DATABASE\_PASSWORD: password for user account to access NFIRS database
    - NFIRS\_DATABASE\_PORT: port number to access NFIRS database
    - NFIRS\_DATABASE\_USER: username for NFIRS database
1. Edit /NFIRSanalysis/NFIRSanalysissetup.sql and replace the table name with the relevant table name found
2. Execute the following command in NFIRSanalysis: psql -U (username) -d (databasename) -a -f NFIRSanalysissetup.sql  This command builds a new NFIRS table whose name matches the second name given in aoo-ifi-hs-mapping.yaml. With default settings, this table represents all residential building fires in the NFIRS dataset. 
3. run sql-confineassume.py then execute the above command but with confineassumptions.sql as the file
4. run sql-AoO-IFI-mapgen.py then execute the above command but with aoo-ifi-sql-map.sql as the file
5. run sql-imputeprepgen.py, then execute the psql command but with imputepreparation.sql as the file
7. Run the following command: Rscript impute-script.R to execute the imputation procedure. This step requires enough memory capacity to hold ~4 copies of the NFIRS database in memory at one time. As of creating this model, between 8-16 gb of free memory is sufficient.
8. delete aooifiprobs.p, and then follow step 8. onward in the instructions above.
