from __future__ import division
import numpy as np
import os
from scipy.interpolate import interp1d
from rdp import rdp


def check_header(filename):
    with open(filename) as f:
        first = f.read(1)
    return first not in '.-0123456789'
def read_hrr(csvfile):
    """return a numpy array of the heat release rate data
    
    Note that incoming heat release rate data must be in the form of 
    time, temperature, with optional one-row header delineating these
    columns
    """
    if check_header(csvfile):
        ret = np.loadtxt(csvfile,delimiter=',',skiprows=1)
    else:
        ret = np.loadtxt(csvfile,delimiter=',')
    return ret

def simplify_hrr(read_hrrdata, npoints=150,maxiter=100):
    """simplify hrr curve using Ramer-Douglas-Peucker algorithm
    
    Note: iteratively applies rdp until number of points in curve is below 
    npoints
    """
    totpoints = read_hrrdata.shape[0]
    newdata = read_hrrdata
    epsilinit = 0.01*np.max(read_hrrdata[:,1])-np.min(read_hrrdata[:,1])
    epsil= epsilinit
    niter = 0
    if(totpoints <= npoints):
        print "Data already below npoints threshold"
        return read_hrrdata
    else:
        while(totpoints > npoints):
            newdata = rdp(newdata,epsilon=epsil)
            print totpoints
            totpoints = newdata.shape[0]
            epsil = epsil + epsilinit
            niter+=1
            if niter >= maxiter:
                print "breaking after {} iterations".format(str(niter))
                break
        print "procedure completed in {} iterations".format(str(niter))
        print "final value of epsilon is {}".format(str(epsil))
        return newdata

def smooth_hrr(read_hrrdata,kind='slinear'):
    """generates a linear spline-smoothed version of the HRR curve
    
    Note: this is just a wrapper for a scipy interp1d function object
    """
    return interp1d(read_hrrdata[:,0],read_hrrdata[:,1],bounds_error=False,fill_value=0)

def ps_radius(qdot,heatflux,radiativefraction=0.3):
    """calculate point source radius corresponding to qdot and heatflux"""
    return np.sqrt(qdot*radiativefraction/(4*np.pi*heatflux))

def ps_qdot(radius,heatflux,radiativefraction=0.3):
    """calculate point source qdot corresponding to radius and heatflux"""
    if radius <=0:
        return 0
    else:
        return (heatflux*4*np.pi*radius**2)/radiativefraction

def timefind(qdot,hrrdata,smooth=True):
    """locate first time in curve corresponding to qdot"""
    if(qdot == np.nan):
        return np.nan
    if smooth: 
        z = smooth_hrr(hrrdata)
        x = np.arange(0,np.max(hrrdata[:,0]),1)
        y = z(x)
        if(np.any(y > qdot)):
            return x[y>qdot][0]
        else:
            return np.nan

"""need a way to calculate radius given qdot and q''
   need a way to calculate qdot given radius and q''
   need a way to locate first time instance on HRR curve
   of given qdot"""
