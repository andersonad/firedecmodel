# coding: utf-8

import subprocess
import yaml
import pdb
import numpy as np
import cPickle as pickle
import glob
import os
import pandas as pd
import csv
import re

def natural_sort(l): 
    convert = lambda text: int(text) if text.isdigit() else text.lower() 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)

#Cfast section header insert function
def cfastsechead(outputstring,headertitle):
    outputstring.append('!!')
    outputstring.append('!!{}'.format(headertitle))
    outputstring.append('!!')
    
#cfast firebuilder function
def cfastfirebuild(roomno,x,y,z,igcond,setvalue,targetref,name,hrrcurve,reff=3.0,chemilist=None):
    """builds the FIRE declaration lines for a CFAST output file and appends them to an outputstring
    
       Note: chemistry is assumed to be methane if a list of values is not supplied
       Note: hrrcurve is assumed to be given in time(s) and hrr(kW)
       firedeclarelist is constructed as follows:
       [FIRE,'roomno','x','y','z','1','igcond','setvalue','targetref','0','0','name']
    """
    reffcurve = hrrcurve[:,1]/np.max(hrrcurve[:,1])*reff
    reffcurve[reffcurve == 0] = 0.05
    outcurve = hrrcurve[:,1]*1000. #convert to Watts for CFAST input
    zerovec = [0]*len(hrrcurve[:,0])
    outputstring = []
    outputstring.append('!!'+name)
    outputstring.append(','.join(['FIRE',','.join([str(i) for i in [roomno,x,y,z,1,igcond, #FIRE line
                                                                     setvalue,targetref,0,0,name]])]))
    if chemilist:
        outputstring.append(','.join(['CHEMI',','.join(chemilist)]))
    else:
        outputstring.append("""CHEMI,1,4,0,0,0,0.3,5E+07""")
    outputstring.append(','.join(['TIME',','.join([str(i) for i in hrrcurve[:,0]])])) #TIME line
    outputstring.append(','.join(['HRR',','.join([str(i) for i in outcurve])])) #HRR line
    outputstring.append(','.join(['SOOT',','.join([str(i) for i in zerovec])])) #SOOT line
    outputstring.append(','.join(['CO',','.join([str(i) for i in zerovec])])) #CO line
    outputstring.append(','.join(['TRACE',','.join([str(i) for i in zerovec])])) #TRACE line
    outputstring.append(','.join(['AREA',','.join([str(i) for i in reffcurve])])) #AREA line
    outputstring.append(','.join(['HEIGH',','.join([str(i) for i in zerovec])])) #HEIGH line
    return '\n'.join(outputstring) #stitch the above together with \n so they will output properly

#build cfastextract class (first seen in FDSconverter)
#include two-way dictionary dependency
class TwoWay:
    def __init__(self):
       self.d = {}
    def __len__(self):
        return(len(self.d)/2)
    def add(self, k, v):
       self.d[k] = v
       self.d[v] = k
    def remove(self, k):
       self.d.pop(self.d.pop(k))
    def get(self, k):
       return self.d[k]

class cfastExtract(object):
    def __init__(self,inputfile):
        self.times = None
        self.ambtemp = None
        self.ambpres = None
        self.elevation = None
        self.ambhumid = None
        self.eamb = None
        self.materials = {}
        self.rooms = {}
        self.hvents = {}
        self.vvents = {}
        self.fires = {}
        self.targets = {}
        self.ramps = TwoWay()
        self.chemi = TwoWay()
        with open(inputfile,'rb') as f:
            roomno=1
            ventno=1
            chemino=1
            rampno=1
            firename=None
            for row in f:
                rowline = [a.strip() for a in row.split(',')]
                if rowline[0]=='TIMES':
                    self.times = tuple(rowline[1:])
                if rowline[0]=='EAMB':
                    self.eamb = tuple(rowline[1:])
                if rowline[0]=='TAMB':
                    self.ambtemp = float(rowline[1])
                    self.ambpressure = float(rowline[2])
                    self.elevation = float(rowline[3])
                    self.ambhumid = float(rowline[4])
                if rowline[0]=='MATL':
                    matname = rowline[1]
                    rowlinereader = csv.reader(row.splitlines())
                    matrow = rowlinereader.next()[2:]
                    self.materials[matname] = tuple(matrow)
                if rowline[0]=='COMPA':
                    self.rooms[str(roomno)] = tuple(rowline[1:])
                    roomno +=1
                if rowline[0]=='HVENT':
                    self.hvents[ventno] = rowline[1:]
                    ventno +=1
                if rowline[0]=='FIRE':
                    firename=rowline[11]
                    firerow=tuple(rowline[1:11])
                if rowline[0]=='CHEMI':
                    chemirow = tuple(rowline[1:])
                    if chemirow not in self.chemi.d.keys():
                        cheminame='chemirow'+str(chemino)
                        self.chemi.add(chemirow,'chemirow'+str(chemino))
                        chemino+=1
                    else:
                        cheminame=self.chemi.get(chemirow)
                if rowline[0]=='TIME':
                    timerow = tuple(rowline[1:])
                if rowline[0]=='HRR':
                    hrr_row = tuple(rowline[1:])
                    ramprow = (timerow,hrr_row)
                    if ramprow not in self.ramps.d.keys():
                        rampname='ramprow'+str(rampno)
                        self.ramps.add(ramprow,'ramprow'+str(rampno))
                        rampno+=1
                    else:
                        rampname=self.ramps.get(ramprow)
                    self.fires[firename]=firerow+(cheminame,)+(rampname,)
                if rowline[0]=='TARGET':
                    targetname = rowline[12]
                    targetrow = tuple(rowline[1:12])
                    self.targets[targetname]=targetrow


def main(configloc,configkey):

    configs = yaml.load(open(configloc))
    configuse = configs[configkey]

    #specify output directory
    outputdir = os.path.abspath(configuse['outputdir'])

    #gather CFAST model list
    cfastmodellist = glob.glob(configuse['cfastmodellist'])
    #import hrr curve to item mapping
    hrrdic = pickle.load(open(os.path.abspath(configuse['hrrdic']),'rb'))
    #import flamespread timing data
    fsdic = pickle.load(open(os.path.abspath(configuse['fsdic']),'rb'))
    #import index reference dictionary
    refdic = pickle.load(open(os.path.abspath(configuse['refdic']),'rb'))
    #set ignitability for heat flux ignition
    ignitability = configuse['igcondvalue'] #W/m^2
    #set compatibility mode for CFAST 7.2 HVENT specification
    compatibility = configuse['compatibility']
    #Detail whether base model input files are coming from CFAST 7.2 specification
    cfast72 = configuse['cfast72']
    #set leakage area around external vents (initial opening fraction prior to activation)
    leakfraction = configuse['leakfraction']
    finalopenfraction = configuse['finalopenfraction']
    #[Optional] override native model time rows with a specified time row
    overridetimerow=configuse['overridetimerow'] #e.g. a 1 hour simulation with 10 second outputs is [3600,50,10,10]

    #map room titles from fsdic (flamespread dictionary) to cfast room names
    roommapdic = configuse['roommapdic']


    cfastmodeldic = {}
    for filename in cfastmodellist:
        basename = os.path.splitext(os.path.basename(filename))[0]
        cfastmodeldic[basename] = cfastExtract(filename)

    targets = {}
    for name in cfastmodeldic.keys():
        print name
        cfastmd = cfastmodeldic[name]
        targets[name] = {}
        for roomno in cfastmd.rooms.keys():
            roomline = cfastmd.rooms[roomno]
            targetname = ''.join([roomline[0],'floortarget'])
          #  !!'roomno','x','y','z','normal_depth','normal_breadth','normal_height','matl','solvetype','solveeq','label'
            targets[name][cfastmd.rooms[roomno][0]] = [roomno,0.01*float(roomline[1]),0.01*float(roomline[2]),0,0,0,1,'GYPSUM','EXPLICIT','PDE',0.5,targetname]
    # %%prun 
    bedA = 0
    bedC = 0
    livroom = 0
    mbed = 0
    i=0
    for name in cfastmodeldic.keys():
        print name
        cfastmd = cfastmodeldic[name] #Variable declare
        for roomno in natural_sort(cfastmd.rooms.keys()):
            fullroomname = cfastmd.rooms[roomno][0] #Variable declare
            roomname = ''.join(i for i in fullroomname if not i.isdigit()) #Variable declare
    #         print roomname
            for roomtype in fsdic.keys():
                roomtypenoslash = roomtype.replace('/','') #Variable declare
    #             print roomtypenoslash
                if(roommapdic[roomtype]==roomname):
                    for item in fsdic[roomtype]:
    #                     print item
                        for rowind,row in enumerate(fsdic[roomtype][item]):
                            if not np.all(np.isnan(row)):
                                outputfilename = '.'.join(['-'.join(str(i) for i in [name,fullroomname,roomtypenoslash,item,rowind]),'in'])
                                #begin cfast output file work here
                                cfastout = [] # use a giant list to generate the output file
                                cfastout.append('VERSN,7,CFAST Simulation')
                                #Scenario keywords
                                cfastsechead(cfastout,'Scenario Configuration Keywords')
                                if overridetimerow:
                                    cfastout.append(','.join(['TIMES',','.join(str(i) for i in overridetimerow)]))
                                else:
                                    cfastout.append(','.join(['TIMES',','.join(cfastmd.times)]))
                                cfastout.append(','.join(['EAMB',','.join(cfastmd.eamb)]))
                                cfastout.append(','.join(['TAMB',','.join(str(i) for i in [cfastmd.ambtemp,cfastmd.ambpressure,
                                                                                           cfastmd.elevation,cfastmd.ambhumid])]))
                                #Material properties
                                cfastsechead(cfastout,'Material Properties')
                                for mat in cfastmd.materials:
                                    cfastout.append(','.join(['MATL',mat,','.join(cfastmd.materials[mat])]))
                                #Compartment keywords
                                cfastsechead(cfastout,'Compartment keywords')
                                for room in natural_sort(cfastmd.rooms.keys()):
                                    cfastout.append(','.join(['COMPA',','.join(cfastmd.rooms[room])]))
                                #Vent keywords
                                cfastsechead(cfastout,'Vent keywords')
                                for hvent in cfastmd.hvents:
                                    outside = max([int(i) for i in cfastmd.rooms.keys()])+1
                                    ventitems = cfastmd.hvents[hvent]
                                    #print ventitems
                                    targetref = ''
                                    igcond = 'TIME'
                                    condval = ''
                                    starttime = '0'
                                    endtime = '0'
                                    initialopen = '1'
                                    finalopen = '1'
                                    if(int(ventitems[0])==outside or int(ventitems[1])==outside):
                                    #    print "exterior vent detected, vent#{}".format(hvent)
                                    #    print "corresponding room number is #{}".format(next((x for x in ventitems[:2] if int(x)!=outside)))
                                        extroomno = next((x for x in ventitems[:2] if x!=outside))
                                        extroomname = cfastmd.rooms[extroomno][0]
                                        targetref = targets[name][extroomname][11]
                                        igcond = 'FLUX'
                                        condval = str(ignitability)
                                        if leakfraction:
                                            initialopen = str(leakfraction)
                                        if finalopen:
                                            finalopen = str(finalopenfraction)
                                    if compatibility:
                                        #alter standard inputs into the new specification re-using code from fires below.
                                        ventitemsaltered = (ventitems[:7]+list('0')+list(ventitems[7])+ 
                                            [igcond,condval,targetref,starttime,initialopen,
                                             endtime,finalopen])
                                    #    print ventitemsaltered
                                        cfastout.append(','.join(['HVENT',','.join(ventitemsaltered)]))
                                    elif cfast72 is True:
                                        ventitemsaltered = (ventitems[:7]+list('0')+list(ventitems[8])+ 
                                            [igcond,condval,targetref,starttime,initialopen,
                                             endtime,finalopen])
                                        cfastout.append(','.join(['HVENT',','.join(ventitemsaltered)]))
                                    else:
                                        cfastout.append(','.join(['HVENT',','.join(cfastmd.hvents[hvent])]))
                                #Fire keywords
                                cfastsechead(cfastout,'Fire keywords')
                                #Room of origin fires
                                for colind,column in enumerate(row):
                                    refitem = refdic[roomtype][colind]
                                    roomitem = refitem[0]
                                    xloc = float(cfastmd.rooms[roomno][1])/2
                                    yloc = float(cfastmd.rooms[roomno][2])/2
                                    zloc = 0.05
                                    if np.isnan(column):
                                        igcond = 'FLUX'
                                        setvalue = ignitability
                                        targetref = targets[name][fullroomname][11]
                                    else:
                                        igcond = 'TIME'
                                        setvalue = column
                                        targetref = 0
                                    cfastout.append(cfastfirebuild(roomno,xloc,yloc,zloc,igcond,setvalue,targetref,
                                                   roomitem,hrrdic[''.join(i for i in roomitem if not i.isdigit())],
                                                                   float(refdic[roomtype][colind][1])))
                                #Room beyond origin flashover fires MANUAL INPUTS REQUIRED HERE, PAY ATTENTION
                                for flashroom in cfastmd.rooms.keys():
                                    if cfastmd.rooms[flashroom][0] == fullroomname:
                                        continue
                                    flashroomname = ''.join(i for i in cfastmd.rooms[flashroom][0] if not i.isdigit())
                                    xloc = float(cfastmd.rooms[flashroom][1])/2
                                    yloc = float(cfastmd.rooms[flashroom][2])/2
                                    zloc = 0.05
                                    igcond = 'FLUX' #need to know that FLUX is the cfast line here
                                    setvalue = ignitability
                                    targetref = targets[name][cfastmd.rooms[flashroom][0]][11]
                                    #This is hardcoded which is dumb, but bear with me here
                                    #We will insert 1 flashover fire in every bedroom and living room that is not the
                                    #room of origin
                                    if flashroomname in ('bedrm','mbedrm'): #use the bedroom flash fire
                                        cfastout.append(cfastfirebuild(flashroom,xloc,yloc,zloc,igcond,setvalue,
                                                        targetref,''.join([cfastmd.rooms[flashroom][0],'flash']),
                                                        hrrdic['bedrmflash'])) #need to magically know that hrrdic key
                                    if flashroomname == 'livingroom': #use the livingroom flash fire
                                        cfastout.append(cfastfirebuild(flashroom,xloc,yloc,zloc,igcond,setvalue,
                                                        targetref,''.join([cfastmd.rooms[flashroom][0],'flash']),
                                                        hrrdic['livingroomflash'])) #need to magically know that hrrdic key
                                #Target and detector keywords
                                cfastsechead(cfastout,'Target and detector keywords')
                                for target in targets[name].keys():
                                    cfastout.append(','.join(['TARGET',','.join(str(i) for i in targets[name][target])]))
                                #now the string should be fully constructed for output
                                outputname = os.path.abspath('/'.join([outputdir,outputfilename]))
                                with open(outputname,'wb') as f:
                                    f.writelines("{}\n".format(outputline) for outputline in cfastout)

if __name__ == "__main__":
    repo_dir = subprocess.Popen(['git', 'rev-parse', '--show-toplevel'], stdout=subprocess.PIPE).communicate()[0].rstrip()
    os.chdir(repo_dir)
    filename = os.path.splitext(os.path.basename(__file__))[0]
    main('./modelconfig.yaml',filename)
