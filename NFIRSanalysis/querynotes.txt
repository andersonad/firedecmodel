--create a detached single-family dwelling building fires table

create table onefamdetfires as (
select * from allfire WHERE prop_use = '419' and version = '5.0' and aid not in ('3','4') and (((inc_date between '20020101' and '20071231' and inc_type IN ('111','112','120','121','122','123')) or (inc_date between '20080101' and '20121231' and inc_type in ('111','120','121','122','123')) and struc_type in ('1','2')) OR (inc_type IN ('113','114','115','116','117','118') and (struc_type IN ('1','2') or struc_type is null))));

--create indexes on new table
create index aooifi_index on onefamdetfiresprocimp (area_orig,first_ign);
create index imputation_index on onefamdetfiresprocimp (imputation);

--query onefamdetfiresprocimp table for average percentage contributions
--construct percentage contributions for each imputation
select imputation, area_orig, first_ign, count(*)::numeric/ (select count(*) from onefamdetfiresproc) as perc from onefamdetfiresprocimp group by imputation, area_orig, first_ign;
--check total contributions across each imputation

--average percentage contributions across imputations
select area_orig, first_ign, avg(perc) from (select imputation, area_orig, first_ign, count(*)::numeric / (select count(*) from onefamdetfiresprocimp group by imputation limit 1) as perc from onefamdetfiresprocimp group by imputation, area_orig, first_ign) as t group by area_orig, first_ign order by area_orig, first_ign;
--check total


--create structure fires table not excluding arson

create table buildingfires as (
select * from allfire WHERE version = '5.0' and aid not in ('3','4') and (((inc_date between '20020101' and '20071231' and inc_type IN ('111','112','120','121','122','123')) or (inc_date between '20080101' and '20121231' and inc_type in ('111','120','121','122','123')) and struc_type in ('1','2')) OR (inc_type IN ('113','114','115','116','117','118') and (struc_type IN ('1','2') or struc_type is null))));

--filter the building heights by the high-rise categorization
select (CASE
when bldg_above::numeric >= 7 then 1
when bldg_above::numeric <7 then 0
end) as highrise from buildingfires group by highrise;



--create detached 1 family residential housing table
create table onefamdetfires as (
select * from buildingfires WHERE prop_use = '419');

--queries for data examination
--area of origin
with tot as (
select count(1) from onefamdetfires
)
select area_orig, round(count(*)::numeric/(select * from tot)*100,2) as perc,
sum(count(*)::numeric/(select * from tot)*100) OVER (order by count(*) desc),
from onefamdetfires
group by area_orig order by count(*) desc;

--item first ignited queries
--AoO 24
with tot as (
select count(1) from onefamdetfires where area_orig = '24'
)
select first_ign, round(count(*)::numeric/(select * from tot)*100,2) as perc,
sum(count(*)::numeric/(select * from tot)*100) OVER (order by count(*) desc),
(select * from tot) as total
from onefamdetfires where area_orig = '24'
group by first_ign order by count(*) desc;

--AoO 21
with tot as (
select count(1) from onefamdetfires where area_orig = '21'
)
select first_ign, round(count(*)::numeric/(select * from tot)*100,2) as perc,
sum(count(*)::numeric/(select * from tot)*100) OVER (order by count(*) desc),
(select * from tot) as total
from onefamdetfires where area_orig = '21'
group by first_ign order by count(*) desc;

--AoO 14
with tot as (
select count(1) from onefamdetfires where area_orig = '14'
)
select first_ign, round(count(*)::numeric/(select * from tot)*100,2) as perc,
sum(count(*)::numeric/(select * from tot)*100) OVER (order by count(*) desc),
(select * from tot) as total
from onefamdetfires where area_orig = '14'
group by first_ign order by count(*) desc;

-AoO flexible (change as desired)
with tot as (
select count(1) from onefamdetfires where area_orig = '26'
)
select first_ign, round(count(*)::numeric/(select * from tot)*100,2) as perc,
sum(count(*)::numeric/(select * from tot)*100) OVER (order by count(*) desc),
(select * from tot) as total
from onefamdetfires where area_orig = '26'
group by first_ign order by count(*) desc;


New queries for revisions:

--create new residential structure fires table:
create table marginalstrucfires2 as
select * from marginalstrucfires where struc_type is NULL or struc_type in ('1','2');

delete from marginalstrucfires2 where inc_type in ('111','112','120','121','122','123') and struc_type is NULL;

drop table if exists resstrucfires;
create table resstrucfires as (select * from marginalstrucfires2 where left(lower(prop_use),1) = '4');


--strong assumptions about confined fire incidents
update resstrucfires set heat_sourc = '12', first_ign='76' where inc_type = '113';
update resstrucfires set heat_sourc = '81' where inc_type = '114';
update resstrucfires set heat_sourc = '12', area_orig = '50' where inc_type = '115';
update resstrucfires set heat_sourc = '81' where inc_type = '116';
update resstrucfires set heat_sourc = '12' where inc_type = '117';
update resstrucfires set first_ign = '96' where inc_type = '118';


--create replacement mapping for the coded categories:

--Area of origin #keep 24 separate
update resstrucfires set area_orig = regexp_replace(area_orig,'^(01|02|03|04|05)$','09');
update resstrucfires set area_orig = regexp_replace(area_orig,'^(11|12|13|14|15|16|17)$','10');
update resstrucfires set area_orig = regexp_replace(area_orig,'^(22|23|25|26|27|28)$','20');
update resstrucfires set area_orig = regexp_replace(area_orig,'^(30|31|32|33|34|35|36|37|38)$','00'); --threw error?
update resstrucfires set area_orig = regexp_replace(area_orig,'^(41|42|43|44|45|46|47)$','40');
update resstrucfires set area_orig = regexp_replace(area_orig,'^(51|52|53|54|55|56|57|58)$','50');
update resstrucfires set area_orig = regexp_replace(area_orig,'^(61|63|64|65|66|67|68)$','60');
update resstrucfires set area_orig = '50' where area_orig = '60'; 
update resstrucfires set area_orig = regexp_replace(area_orig,'^(73|74|75)$','71');
update resstrucfires set area_orig = regexp_replace(area_orig,'^(72|76|77|78)$','70');
update resstrucfires set area_orig = regexp_replace(area_orig,'^(80|81|82|83|84|85|86|90|91|92|93|94|95|96|97|98)$','00');
update resstrucfires set area_orig = regexp_replace(area_orig,'^(| |uu)$','UU');

--Item first ignited #keep 76 separate
update resstrucfires set first_ign = regexp_replace(first_ign,'(^1$|^8$)','00');
update resstrucfires set first_ign = regexp_replace(first_ign,'^(11|12|13|14|15|16|17|18)$','10');
update resstrucfires set first_ign = regexp_replace(first_ign,'^(22|23|24|25|26)$','20');
update resstrucfires set first_ign = regexp_replace(first_ign,'^(33|34|36|37|38)$','30');
update resstrucfires set first_ign = '31' where first_ign = '32';
update resstrucfires set first_ign = regexp_replace(first_ign,'^(41|42|43|44|45|46|47|40)$','00');
update resstrucfires set first_ign = regexp_replace(first_ign,'^(51|52|53|54|55|56|57|58|59)$','50');
update resstrucfires set first_ign = regexp_replace(first_ign,'^(61|62|63|64)$','65');
update resstrucfires set first_ign = regexp_replace(first_ign,'^(66|67|68|60)$','00');
update resstrucfires set first_ign = regexp_replace(first_ign,'^(71|72|73|74|75|77|70)$','00');
update resstrucfires set first_ign = regexp_replace(first_ign,'^(80|82|83|84|85|86|87|88)$','00');
update resstrucfires set first_ign = regexp_replace(first_ign,'^(91)$','92');
update resstrucfires set first_ign = regexp_replace(first_ign,'^(93|94)$','95');
update resstrucfires set first_ign = regexp_replace(first_ign,'^(90|96|97|99)$','00');
update resstrucfires set first_ign = regexp_replace(first_ign,'^(| |uu)$','UU');

#Heat source
##for heat source we conceptually desire the following:
##low energy
##smoking materials
##partial unknowns
##total unknowns
##other heat sources

update resstrucfires set heat_sourc = regexp_replace(heat_sourc,'^(1|4)$','00');
update resstrucfires set heat_sourc = regexp_replace(heat_sourc,'^(61|62|63)$','61');
update resstrucfires set heat_sourc = regexp_replace(heat_sourc,'^(64|65|66|69)$','64');
update resstrucfires set heat_sourc = regexp_replace(heat_sourc,'^(11|12|10|40|41|43|83)$','40');
update resstrucfires set heat_sourc = regexp_replace(heat_sourc,'^(13|42|51|53|54|55|56|67|68|70|71|72|73|74|81|82|84|80|97|90)$','00');
update resstrucfires set heat_sourc = regexp_replace(heat_sourc,'^(| |uu)$','UU');

--next we want a column for the hour of the day
alter table resstrucfires add column hour text;
update resstrucfires set hour = date_part('hour',alarm);


--next we want a column for season of the year (month category)
##summer is June, July, August
##Fall is September, October, November
##winter is December, January, February
##spring is March, April, May
alter table resstrucfires add column season text;
update resstrucfires set season = date_part('month',alarm);
update resstrucfires set season = regexp_replace(season,'^(12|1|2)$','winter');
update resstrucfires set season = regexp_replace(season,'^(3|4|5)$','spring');
update resstrucfires set season = regexp_replace(season,'^(6|7|8)$','summer');
update resstrucfires set season = regexp_replace(season,'^(9|10|11)$','fall');

--Finally we need to make a unique ID for all resstrucfires so we don't have to carry around 5 indicators
alter table resstrucfires add column fireid serial unique;

--add these fireids to the civiliancasualtyback column
alter table civiliancasualtyback add column fireid int;
update civiliancasualtyback l set fireid = r.fireid from resstrucfires r where l.state = r.state and
l.fdid = r.fdid and l.inc_date = r.inc_date and l.inc_no = r.inc_no and l.exp_no = r.exp_no;


--finally climate regions of the united states
alter table resstrucfires add column climate_reg text;
update resstrucfires set climate_reg = state;
update resstrucfires set climate_reg = regexp_replace(climate_reg,'^(IL|IN|KY|MO|OH|TN|WV)$','central');
update resstrucfires set climate_reg = regexp_replace(climate_reg,'^(IA|MI|MN|WI)$','eastnorthcentral');
update resstrucfires set climate_reg = regexp_replace(climate_reg,'^(CT|DE|ME|MD|MA|NH|NJ|NY|PA|RI|VT)$','northeast');
update resstrucfires set climate_reg = regexp_replace(climate_reg,'^(ID|OR|WA)$','northwest');
update resstrucfires set climate_reg = regexp_replace(climate_reg,'^(AR|KS|LA|MS|OK|TX)$','south');
update resstrucfires set climate_reg = regexp_replace(climate_reg,'^(AL|FL|GA|NC|SC|VA)$','southeast');
update resstrucfires set climate_reg = regexp_replace(climate_reg,'^(AZ|CO|NM|UT)$','southwest');
update resstrucfires set climate_reg = regexp_replace(climate_reg,'^(CA|NV)$','west');
update resstrucfires set climate_reg = regexp_replace(climate_reg,'^(MT|NE|ND|SD|WY)$','westnorthcentral');
update resstrucfires set climate_reg = 'southwest' where climate_reg in ('HI','MP','NA');
update resstrucfires set climate_reg = 'northeast' where climate_reg in ('DC');
update resstrucfires set climate_reg = 'southeast' where climate_reg in ('PR');
update resstrucfires set climate_reg = 'northwest' where climate_reg in ('AK');

\copy (select fireid, date_part('year',inc_date) as year, inc_type, climate_reg, season, hour, heat_sourc, first_ign, area_orig from resstrucfires) to ~/resstrucfireimpute.csv with CSV HEADER;

create table resstrucimputed (
	fireid int,
	year int,
	inc_type varchar(3),
	climate_reg text,
	season text,
	hour int,
	heat_sourc varchar(2),
	first_ign varchar(2),
	area_orig varchar(2),
	imputation varchar(2)
);

--this is for the UTFRG server
COPY resstrucimputed from '/home/ignoranceman/resfireimputed.csv' with CSV HEADER;

--add various relevant columns back to resstrucimputed
alter table resstrucimputed add column equip_inv varchar(3);
alter table resstrucimputed add column eq_power varchar(2);
update resstrucimputed l set equip_inv = r.equip_inv, eq_power = r.eq_power from resstrucfires r where l.fireid=r.fireid;
alter table resstrucimputed add column oth_death text;
alter table resstrucimputed add column oth_inj text;
update resstrucimputed l set oth_death = r.oth_death, oth_inj = r.oth_inj from resstrucfires r where l.fireid=r.fireid;
alter table resstrucimputed add prop_loss text, add cont_loss text;
update resstrucimputed l set prop_loss = r.prop_loss, cont_loss = r.cont_loss from resstrucfires r where l.fireid=r.fireid;

drop table if exists fatalanalysis;
create table fatalanalysis as
(select * from resstrucimputed inner join civiliancasualtyback using (fireid));

--finally export only the imputations from fatalanalysis for "open flame ignition" and smoking materials fires.
\copy (select * from fatalanalysis where sev = '5' and exp_no='0' and (heat_sourc = '67' or (heat_sourc in ('64','65','66') or (equip_inv = '872' and eq_power in ('20','21','22','31','32','33','34','30')) or equip_inv = '873') or heat_sourc = '43' or equip_inv in ('141','142','143','144','145') or heat_sourc in ('61','62','63'))) to ~/fatalanalysis.csv CSV HEADER;

--create a british fires comparable table.

--let's start by figuring out just the smoking fires, injuries, fatalities
select year, avg(smokfir) as smokfir, avg(smokfat) as smokfat, avg(smokinj) as smokinj from (select year, imputation, count(*) as smokfir, sum(oth_death::integer) as smokfat, sum(oth_inj::integer) as smokinj from resstrucimputed where heat_sourc in ('61','62','63') group by year, imputation) as temp group by year order by year desc;

--now just openflame/space heating fires

select year, avg(opfir) as opfir, avg(opfat) as opfat, avg(opinj) as opinj from (select year, imputation, count(*) as opfir, sum(oth_death::integer) as opfat, sum(oth_inj::integer) as opinj from resstrucimputed where (heat_sourc = '67' or (heat_sourc in ('64','65','66') or (equip_inv = '872' and eq_power in ('20','21','22','31','32','33','34','30')) or equip_inv = '873') or heat_sourc = '43' or equip_inv in ('141','142','143','144','145')) group by year, imputation) as temp group by year order by year desc;

--finally just openflame fires

select year, avg(comfir) as comfir, avg(comfat) as comfat, avg(cominj) as cominj from (select year, imputation, count(*) as comfir, sum(oth_death::integer) as comfat, sum(oth_inj::integer) as cominj from resstrucimputed where heat_sourc = '67' or (heat_sourc in ('64','65','66') or (equip_inv = '872' and eq_power in ('20','21','22','31','32','33','34','30')) or equip_inv = '873') or heat_sourc = '43' group by year, imputation) as temp group by year order by year desc;

--lastly gather the total numbers of the three categories
select date_part('year',inc_date) as year, count(*) as totfir, sum(oth_death::integer) as totfat, sum(oth_inj::integer) as totinj from resstrucfires group by year order by year desc;

--sanity check on imputations, totals are correctly preserved!

select year, avg(totfir) as totfir, avg(totfat) as totfat, avg(totinj) as totinj from (select year, imputation, count(*) as totfir, sum(oth_death::integer) as totfat, sum(oth_inj::integer) as totinj from resstrucimputed group by year, imputation) as temp group by year order by year desc;

--now let's merge everything into one big table call
drop table if exists figure1data;
create table figure1data as (
with smoke as (
select year, round(avg(smokfir),0) as smokfir, round(avg(smokfat),0) as smokfat, round(avg(smokinj),0) as smokinj from (select year, imputation, count(*) as smokfir, sum(oth_death::integer) as smokfat, sum(oth_inj::integer) as smokinj from resstrucimputed where heat_sourc in ('61','62','63') group by year, imputation) as temp group by year order by year desc), allflame as (
select year, round(avg(opfir),0) as opfir, round(avg(opfat),0) as opfat, round(avg(opinj),0) as opinj from (select year, imputation, count(*) as opfir, sum(oth_death::integer) as opfat, sum(oth_inj::integer) as opinj from resstrucimputed where (heat_sourc = '67' or (heat_sourc in ('64','65','66') or (equip_inv = '872' and eq_power in ('20','21','22','31','32','33','34','30')) or equip_inv = '873') or heat_sourc = '43' or equip_inv in ('141','142','143','144','145')) group by year, imputation) as temp group by year order by year desc), openflame as (
select year, round(avg(comfir),0) as comfir, round(avg(comfat),0) as comfat, round(avg(cominj),0) as cominj from (select year, imputation, count(*) as comfir, sum(oth_death::integer) as comfat, sum(oth_inj::integer) as cominj from resstrucimputed where heat_sourc = '67' or (heat_sourc in ('64','65','66') or (equip_inv = '872' and eq_power in ('20','21','22','31','32','33','34','30')) or equip_inv = '873') or heat_sourc = '43' group by year, imputation) as temp group by year order by year desc), total as (
select year, round(avg(totfir),0) as totfir, round(avg(totfat),0) as totfat, round(avg(totinj),0) as totinj from (select year, imputation, count(*) as totfir, sum(oth_death::integer) as totfat, sum(oth_inj::integer) as totinj from resstrucimputed group by year, imputation) as temp group by year order by year desc)

select * from smoke inner join allflame using (year) inner join openflame using (year) inner join total using (year));


--now the hourly version
drop table if exists figure7data;
create table figure7data as (
with smoke as (
select hour, round(avg(smokfir),0) as smokfir, round(avg(smokfat),0) as smokfat, round(avg(smokinj),0) as smokinj from (select hour, imputation, count(*) as smokfir, sum(oth_death::integer) as smokfat, sum(oth_inj::integer) as smokinj from resstrucimputed where heat_sourc in ('61','62','63') group by hour, imputation) as temp group by hour order by hour desc), allflame as (
select hour, round(avg(opfir),0) as opfir, round(avg(opfat),0) as opfat, round(avg(opinj),0) as opinj from (select hour, imputation, count(*) as opfir, sum(oth_death::integer) as opfat, sum(oth_inj::integer) as opinj from resstrucimputed where (heat_sourc = '67' or (heat_sourc in ('64','65','66') or (equip_inv = '872' and eq_power in ('20','21','22','31','32','33','34','30')) or equip_inv = '873') or heat_sourc = '43' or equip_inv in ('141','142','143','144','145')) group by hour, imputation) as temp group by hour order by hour desc), openflame as (
select hour, round(avg(comfir),0) as comfir, round(avg(comfat),0) as comfat, round(avg(cominj),0) as cominj from (select hour, imputation, count(*) as comfir, sum(oth_death::integer) as comfat, sum(oth_inj::integer) as cominj from resstrucimputed where heat_sourc = '67' or (heat_sourc in ('64','65','66') or (equip_inv = '872' and eq_power in ('20','21','22','31','32','33','34','30')) or equip_inv = '873') or heat_sourc = '43' group by hour, imputation) as temp group by hour order by hour desc), total as (
select hour, round(avg(totfir),0) as totfir, round(avg(totfat),0) as totfat, round(avg(totinj),0) as totinj from (select hour, imputation, count(*) as totfir, sum(oth_death::integer) as totfat, sum(oth_inj::integer) as totinj from resstrucimputed group by hour, imputation) as temp group by hour order by hour desc)

select * from smoke inner join allflame using (hour) inner join openflame using (hour) inner join total using (hour));

--finally the loss version

drop table if exists figure8data;
create table figure8data as (
with smoke as (
select year, round(avg(smokloss),0) as smokloss from (select year, imputation, sum(prop_loss::integer+cont_loss::integer) as smokloss from resstrucimputed where heat_sourc in ('61','62','63') group by year, imputation) as temp group by year order by year desc), allflame as (
select year, round(avg(oploss),0) as oploss from (select year, imputation, sum(prop_loss::integer+cont_loss::integer) as oploss from resstrucimputed where (heat_sourc = '67' or (heat_sourc in ('64','65','66') or (equip_inv = '872' and eq_power in ('20','21','22','31','32','33','34','30')) or equip_inv = '873') or heat_sourc = '43' or equip_inv in ('141','142','143','144','145')) group by year, imputation) as temp group by year order by year desc), total as (
select year, round(avg(totloss),0) as totloss from (select year, imputation, sum(prop_loss::integer+cont_loss::integer) as totloss from resstrucimputed group by year, imputation) as temp group by year order by year desc)

select * from smoke inner join allflame using (year) inner join total using (year));


--next, first item ignited collation

drop table if exists table2data;
create table table2data as (
with smoke as (
select first_ign, round(avg(smokfir),0) as smokfir, round(avg(smokfat),0) as smokfat, round(avg(smokinj),0) as smokinj from (select first_ign, imputation, count(*) as smokfir, sum(oth_death::integer) as smokfat, sum(oth_inj::integer) as smokinj from resstrucimputed where heat_sourc in ('61','62','63') group by first_ign, imputation) as temp group by first_ign order by first_ign desc), allflame as (
select first_ign, round(avg(opfir),0) as opfir, round(avg(opfat),0) as opfat, round(avg(opinj),0) as opinj from (select first_ign, imputation, count(*) as opfir, sum(oth_death::integer) as opfat, sum(oth_inj::integer) as opinj from resstrucimputed where (heat_sourc = '67' or (heat_sourc in ('64','65','66') or (equip_inv = '872' and eq_power in ('20','21','22','31','32','33','34','30')) or equip_inv = '873') or heat_sourc = '43' or equip_inv in ('141','142','143','144','145')) group by first_ign, imputation) as temp group by first_ign order by first_ign desc), openflame as (
select first_ign, round(avg(comfir),0) as comfir, round(avg(comfat),0) as comfat, round(avg(cominj),0) as cominj from (select first_ign, imputation, count(*) as comfir, sum(oth_death::integer) as comfat, sum(oth_inj::integer) as cominj from resstrucimputed where heat_sourc = '67' or (heat_sourc in ('64','65','66') or (equip_inv = '872' and eq_power in ('20','21','22','31','32','33','34','30')) or equip_inv = '873') or heat_sourc = '43' group by first_ign, imputation) as temp group by first_ign order by first_ign desc), total as (
select first_ign, round(avg(totfir),0) as totfir, round(avg(totfat),0) as totfat, round(avg(totinj),0) as totinj from (select first_ign, imputation, count(*) as totfir, sum(oth_death::integer) as totfat, sum(oth_inj::integer) as totinj from resstrucimputed group by first_ign, imputation) as temp group by first_ign order by first_ign desc)

select * from smoke inner join allflame using (first_ign) inner join openflame using (first_ign) inner join total using (first_ign));


--Table 6: upholstered furniture fatalities with smoking matls or open flame/space heater deaths

--total number

select avg(count) from (select imputation, count(*) from fatalanalysis where first_ign = '21' and sev='5' group by imputation) as temp;

--open flame/space heater

select avg(count) from (select imputation, count(*) from fatalanalysis where (heat_sourc = '67' or (heat_sourc in ('64','65','66') or (equip_inv = '872' and eq_power in ('20','21','22','31','32','33','34','30')) or equip_inv = '873') or heat_sourc = '43' or equip_inv in ('141','142','143','144','145')) and first_ign = '21' and sev='5' and age::numeric <=21 group by imputation) as temp;

--smoking materials

select avg(count) from (select imputation, count(*) from fatalanalysis where heat_sourc in ('61','62','63') and first_ign = '21' and sev='5' and age::numeric <= 21 group by imputation) as temp; 
