import yaml
import subprocess
import imp
import os
import numpy as np
import glob
import cPickle as pickle
import re
import pandas as pd
from itertools import chain
from pint import UnitRegistry

def natural_sort(l): 
    convert = lambda text: int(text) if text.isdigit() else text.lower() 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)

def main(configlocation,configkey):

    ureg = UnitRegistry()
    Q_ = ureg.Quantity
    configs = yaml.load(open(configlocation))
    configuse = configs[configkey]
    #the next line is a module import so this is bad behavior, but we need the right cwd to leave it be
    hrr_reader = imp.load_source('hrr_reader',configuse['hrr_readerloc'])


    #Set ignitability of items
    ignitability = configuse['ignitability'] #[Kw/m^2]

    datasources = configuse['datasources']
    filelists = {y:natural_sort([os.path.abspath(x) 
        for x in sorted(glob.glob(configuse['furniturelayoutdataloc'].format(y)))])
        for y in datasources}

    reffvals = pd.read_csv(configuse['reffvals']) #see csv for data structure
                                                #note that room name is important

    #Construct a set of reference items from the reference values
    refset = {''.join(i for i in s if not i.isdigit()) for s in reffvals.item}
    #Construct a nested dictionary that maps, for each room, every index to its
    #corresponding item
    refdic = {i:{a:x for (a,x) in zip(reffvals.indexval.loc[reffvals.room==i],
              reffvals.item.loc[reffvals.room==i])} for i in datasources}
    #Construct a dictionary relating items to heat release rates
    hrrdic = {}
    for item in refset:
        hrrdic[item]=hrr_reader.simplify_hrr(hrr_reader.read_hrr(
            os.path.abspath(configuse['itemhrrdataloc'].format(item))))
    #print len(hrrdic) 
    #print refdic['LivingRoom'].values()


    #for outputarrays, the x-list corresponds to the number of items in room
    outputdic = {}
    for name in datasources:
        outputdic[name] = {itemname:np.full((len(filelists[name]),len(refdic[name])),-1.0) for itemname in refdic[name].values()}
    #print outputdic

    #generate the NxM arrays of distance values
    #N is the number of observations
    #M is the number of pairwise relations between furniture items
    #Note that because of the effective radius of items, these matrices will not
    #end up being symmetric
    for name in datasources:
        for ind,file in enumerate(filelists[name]):
            output = np.loadtxt(file,delimiter=',')
            #determine distance to object surface by subtracting reff
            output = output - reffvals.reff.loc[reffvals.room==name].values
            #print output
            for row in np.arange(0,output.shape[0]):
                itemname = ''.join(i for i in refdic[name][row] if not i.isdigit())  
                for column in np.arange(0,output.shape[1]):
                    value = Q_(output[row,column],ureg.foot).to(ureg.meter).magnitude
                    #if output[row,column] < 0:
                        #print value
                    hrrneed = hrr_reader.ps_qdot(value,ignitability)
                    #if value < 0:
                        #print hrrneed
                    out = hrr_reader.timefind(hrrneed,hrrdic[itemname])
                    outputdic[name][refdic[name][row]][ind,column] = out
    pickle.dump(outputdic,open(configuse['fsdicoutloc'],'wb'))
    #output comprehensive hrrdic
    hrrdic2 = {}
    #print glob.glob('../../itemhrrprocessing/itemhrr/*.csv')
    for item in [os.path.abspath(i) for i in glob.glob(configuse['itemhrrcsvdir'])]:
    #    print item
        itemname = os.path.basename(item).split('.')[0]
    #    print itemname
        hrrdic2[itemname]=hrr_reader.simplify_hrr(hrr_reader.read_hrr(
            os.path.abspath(item)))
    pickle.dump(hrrdic2,open(configuse['hrrdicoutloc'],'wb'))
    #include reff in refdic output
    refdic2 = {i:{a:[x,z] for (a,x,z) in zip(reffvals.indexval.loc[reffvals.room==i],
              reffvals.item.loc[reffvals.room==i],
              reffvals.reff.loc[reffvals.room==i])} 
              for i in datasources}
    pickle.dump(refdic2,open(configuse['refdicoutloc'],'wb'))

if __name__ == "__main__":
    repo_dir = subprocess.Popen(['git', 'rev-parse', '--show-toplevel'], stdout=subprocess.PIPE).communicate()[0].rstrip()
    os.chdir(repo_dir)
    filename = os.path.splitext(os.path.basename(__file__))[0]
    main('./modelconfig.yaml',filename)

