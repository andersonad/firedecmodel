import yaml
import subprocess
import os
import sys

def main(configloc):
    inputconfig = yaml.load(open(configloc))
    jobbuilderscriptloc = inputconfig['jobbuilderscriptloc']
    jobbuildercfastfilename = inputconfig['jobbuildercfastfilename']
    CFASTrunsdirectory = inputconfig['CFASTrunsdirectory']
    cfastexeclocation = inputconfig['cfastexeclocation']
    if not cfastexeclocation:
        raise Exception("Error, no configuration location for CFAST executable specified, please edit cfastmodelrunparams.yaml and specify the 'cfastexeclocation'")
    filerunnerscriptloc = inputconfig['filerunnerscriptloc']
    runlogname = inputconfig['runlogname']
    njobs = inputconfig['njobs']
    timeout = inputconfig['timeout']

    # print inputconfig

    #call jobbuilder for the first time
    subprocess.call([jobbuilderscriptloc, '-t',jobbuildercfastfilename,'-s',
                     CFASTrunsdirectory,'-e','.in',cfastexeclocation])

    #call filerunner to run the jobs with relevant options

    # print njobs
    filerunnercall = [filerunnerscriptloc,'-l',runlogname,jobbuildercfastfilename]
    if njobs:
        filerunnercall.insert(3,njobs)
        filerunnercall.insert(3,'-j')
    if timeout:
        filerunnercall.insert(3,timeout)
        filerunnercall.insert(3,'-t')
    subprocess.call([str(i) for i in filerunnercall])

if __name__ == "__main__":
    repo_dir = subprocess.Popen(['git', 'rev-parse', '--show-toplevel'], stdout=subprocess.PIPE).communicate()[0].rstrip()
    os.chdir(repo_dir)
    if len(sys.argv) == 2:
        configlocation = os.path.abspath(sys.argv[1])
    elif len(sys.argv) > 2:
        raise Exceptions("Too many system arguments submitted to script")
    else:
        configlocation = os.path.abspath('cfastmodelrunparams.yaml')
    main(configlocation)

