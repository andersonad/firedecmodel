#!/bin/bash
#Note that this script only needs to be run once per database for all sensitivity analysis
#Unless the underlying category aggregation assumptions are to be changed.

echo "Running confineassumptions.sql"

PGPASSWORD="$NFIRS_DATABASE_PASSWORD" psql -h "$NFIRS_DATABASE_HOST" -U "$NFIRS_DATABASE_USER" -p "$NFIRS_DATABASE_PORT" -d "$NFIRS_DATABASE_NAME" -a -f confineassumptions.sql >> databaseprep.log

echo "Running aoo-ifi-sql-map.sql"

PGPASSWORD="$NFIRS_DATABASE_PASSWORD" psql -h "$NFIRS_DATABASE_HOST" -U "$NFIRS_DATABASE_USER" -p "$NFIRS_DATABASE_PORT" -d "$NFIRS_DATABASE_NAME" -a -f aoo-ifi-sql-map.sql >> databaseprep.log

echo "Running imputepreparation.sql"

PGPASSWORD="$NFIRS_DATABASE_PASSWORD" psql -h "$NFIRS_DATABASE_HOST" -U "$NFIRS_DATABASE_USER" -p "$NFIRS_DATABASE_PORT" -d "$NFIRS_DATABASE_NAME" -a -f imputepreparation.sql >> databaseprep.log

echo "Running impute-script.R"

Rscript impute-script.R
